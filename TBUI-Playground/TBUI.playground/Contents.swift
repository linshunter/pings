import Foundation
import UIKit

extension UIColor {
    
    convenience init(red: Int, green: Int, blue: Int) {
        
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(hex: Int) {
        
        self.init(red: (hex >> 16) & 0xff, green: (hex >> 8) & 0xff, blue: hex & 0xff)
    }
    
    // MARK: - Custom Colors
    
    class func payQwiqSelectedBlue() -> UIColor {
        
        return UIColor(red: 0, green: 155, blue: 221)
    }
    
    class func backgroundColor() -> UIColor {
        
        return UIColor(red: 230, green: 233, blue: 237)
    }
    
    class func payQwiqDarkBlue() -> UIColor {
        
        return UIColor(red: 5, green: 39, blue: 77)
    }
    
    class func payQwiqBlue() -> UIColor {
        
        return UIColor(red: 0, green: 177, blue: 230)
    }
    
    class func payQwiqBlueButton() -> UIColor {
        
        return UIColor(red: 66, green: 195, blue: 235)
    }
    
    class func payQwiqYellow() -> UIColor {
        
        return UIColor(red: 255, green: 192, blue: 0)
    }
    
    class func payQwiqBrightGreen() -> UIColor {
        
        return UIColor(red: 113, green: 187, blue: 100)
    }
    
    class func payQwiqCoral() -> UIColor {
        
        return UIColor(red: 252, green: 107, blue: 107)
    }
    
    class func payQwiqBannerRed() -> UIColor {
        
        return UIColor(red: 255, green: 130, blue: 128)
    }
    
    class func payQwiqBannerBlue() -> UIColor {
        
        return UIColor(red: 26, green: 92, blue: 119)
    }
    
    class func payQwiqSpinnerBlue() -> UIColor {
        
        return UIColor(red: 0, green: 177, blue: 230)
    }
    
    class func payQwiqEmerald() -> UIColor {
        
        return UIColor(red: 28, green: 92, blue:118)
    }
    
    class func payQwiqMidGrey() -> UIColor {
        
        return UIColor(red: 230, green: 233, blue: 237)
    }
    
    class func payQwiqLightGrey() -> UIColor {
        
        return UIColor(red: 232, green: 232, blue: 232)
    }
    
    class func payQwiqBrightBlue() -> UIColor {
        
        return UIColor(red: 0, green: 161, blue: 224)
    }
    
    class func payQwiqRoundedButtonSelectedBackgroundColor() -> UIColor {
        
        return UIColor(red: 3.0/255.0, green: 108.0/255.0, blue: 147.0/255.0, alpha: 1.0)
    }
    
    class func payQwiqRoundedButtonDeselectedBackgroundColor() -> UIColor {
        
        return UIColor(red: 0/255.0, green: 179/255.0, blue: 234/255.0, alpha: 1.0)
    }
    
    class func payQwiqLightBlue() -> UIColor {
        
        return UIColor(red: 0, green: 179, blue: 234)
    }
    
    class func payQwiqAddCardDetailsErrorMessageArrowBackgroundColor() -> UIColor {
        
        return UIColor(red: 241, green: 241, blue: 241)
    }
    
    class func legalRequirementsErrorViewBorder() -> UIColor {
        
        return UIColor(red: 240, green: 138, blue: 143)
    }
    
    class func legalRequirementsErrorViewBackground() -> UIColor {
        
        return UIColor(red: 252, green: 248, blue: 249)
    }
    
    // MARK: - Custom font colors
    
    class func payQwiqPrimaryMessage() -> UIColor {
        
        return UIColor(hex: 0x00B1E6)
    }
    
    class func payQwiqSecondaryMessage() -> UIColor {
        
        return UIColor(hex: 0x234162)
    }
    
    class func payQwiqMainTitle() -> UIColor {
        
        return UIColor(hex: 0x05274D)
    }
    
    class func payQwiqSectionTitle() -> UIColor {
        
        return UIColor(hex: 0x00B1E6)
    }
    
    class func payQwiqTips() -> UIColor {
        
        return UIColor(hex: 0x434B54)
    }
    
    class func payQwiqActionBarTitle() -> UIColor {
        
        return UIColor(hex: 0xFFFFFF)
    }
    
    class func payQwiqOtherMessage() -> UIColor {
        
        return UIColor(hex: 0x777777)
    }
    
    class func payQwiqButtons() -> UIColor {
        
        return UIColor(hex: 0xFFFFFF)
    }
    
    class func payQwiqInactiveField() -> UIColor {
        
        return UIColor(hex: 0xCCCCCC)
    }
    
    class func payQwiqValidatedField() -> UIColor {
        
        return UIColor(hex: 0x444444)
    }
    
    class func payQwiqErrorField() -> UIColor {
        
        return UIColor(hex: 0xFD8380)
    }
    
    class func payQwiqWhiteLinkButton() -> UIColor {
        
        return UIColor(hex: 0xFFFFFF)
    }
    
    class func payQwiqSignUpMainTitleColor() -> UIColor {
        
        return UIColor(hex: 0x05274D)
    }
    
    class func payQwiqStandardText() -> UIColor {
        
        return UIColor(red: 115, green: 115, blue: 115)
    }
    
    class func payQwiqMidGreenText() -> UIColor {
        
        return UIColor(red: 124, green: 196, blue: 110)
    }
    
    class func payQwiqDarkGreyText() -> UIColor {
        
        return UIColor(red: 130, green: 130, blue: 130)
    }
    
    // MARK: - Custom field colors
    
    class func payQwiqInactiveBackground() -> UIColor {
        
        return UIColor(hex: 0xF8F8F8)
    }
    
    class func payQwiqFocusHighlight() -> UIColor {
        
        return UIColor(hex: 0x00B1E6)
    }
    
    class func payQwiqErrorBackground() -> UIColor {
        
        return UIColor(hex: 0xFFFFFF)
    }
    
    class func payQwiqErrorHighlight() -> UIColor {
        
        return UIColor(hex: 0xFD8380)
    }
    
    class func payQwiqTextFieldPlaceholderTextColor() -> UIColor {
        
        return UIColor(red: 138, green: 146, blue: 159)
    }
    
    // MARK: - Custom control colors
    
    class func payQwiqActionButton() -> UIColor {
        
        return UIColor(hex: 0x00B1E6)
    }
    
    class func payQwiqButtonIconBackground() -> UIColor {
        
        return UIColor(hex: 0x83D0ED)
    }
    
    class func payQwiqActiveButton() -> UIColor {
        
        return UIColor(hex: 0x05274D)
    }
    
    class func payQwiqMainBackground() -> UIColor {
        
        return UIColor(hex: 0xFFFFFF)
    }
    
    class func payQwiqToolTipBackground() -> UIColor {
        
        return UIColor(hex: 0xF1F1F1)
    }
    
    class func payQwiqSecondaryButton() -> UIColor {
        
        return UIColor(hex: 0xC2CAD3)
    }
    
    class func payQwiqDisabledButton() -> UIColor {
        
        return UIColor(hex: 0x64758C)
    }
    
    class func payQwiqLinkButtonBackground() -> UIColor {
        
        return UIColor(hex: 0xFFFFFF)
    }
    
    class func payQwiqWhiteLinkButtonControl() -> UIColor {
        
        return UIColor(hex: 0x0A527F)
    }
    
    class func payQwiqDarkBlueText() -> UIColor {
        
        return UIColor(red: 26.0/255.0, green: 57.0/255.0, blue: 77.0/255.0, alpha: 1.0)
    }
}

struct FontNames {
    
    static let TescoRegular = "Tesco"
    static let TescoBold = "TescoBd"
    static let TescoLight = "TescoLight"
    static let TescoItalic = "TescoItalic"
    static let TescoBlack = "TescoBlack"
    static let TescoBoldItalic = "TescoBoldItalic"
}

extension UIFont {
    
    class func primaryMessage() -> UIFont {
        
        return UIFont.boldSystemFont(ofSize: 17.5)
    }
    
    class func otherMessage() -> UIFont {
        return UIFont(name: FontNames.TescoLight, size: 16)!
    }
    
    class func mainTitle() -> UIFont {
        
        return UIFont.boldSystemFont(ofSize: 18.5)
    }
    
    class func screenTitle() -> UIFont {
        
        return UIFont(name: FontNames.TescoRegular, size: 17.5)!
    }
    
    class func sectionTitle() -> UIFont {
        
        return UIFont(name: FontNames.TescoRegular, size: 17.5)!
    }
    
    class func tips() -> UIFont {
        
        return UIFont(name: FontNames.TescoRegular, size: 16)!
    }
    
    class func tipLink() -> UIFont {
        
        return UIFont(name: FontNames.TescoBold, size: 16)!
    }
    
    class func actionBarNavigation() -> UIFont {
        
        return UIFont(name: FontNames.TescoLight, size: 19)!
    }
    
    class func actionBarTitle() -> UIFont {
        
        return UIFont(name: FontNames.TescoRegular, size: 19)!
    }
    
    class func buttons() -> UIFont {
        
        return UIFont.systemFont(ofSize: 17.5)
    }
    
    class func inactiveField() -> UIFont {
        
        return UIFont(name: FontNames.TescoRegular, size: 16.0)!
    }
    
    class func linkButton() -> UIFont {
        
        return UIFont.systemFont(ofSize: 17.0)
//        return UIFont(name: FontNames.TescoRegular, size: 17.0)!
    }
    
    class func registrationJourneyContinueButton() -> UIFont {
        
        return UIFont(name: FontNames.TescoRegular, size: 20.0)!
    }
}

extension Dictionary {
    
    func stringValueForKey(key: String) -> String {
        
        if let dictionaryKey = key as? Key {
            
            if let value = self[dictionaryKey] {
                
                if String(describing: value) == "nil" {
                    
                    return ""
                }
                
                if let stringValue = value as? NSString {
                    
                    return stringValue as String
                }
            }
        }
        
        return ""
    }
    
    func intValueForKey(key: String) -> Int {
        
        if let dictionaryKey = key as? Key {
            
            if let intValue = self[dictionaryKey] as? Int {
                
                return intValue
            }
        }
        
        return 0
    }
    
    func boolValueForKey(key: String) -> Bool {
        
        if let dictionaryKey = key as? Key {
            
            if let boolValue = self[dictionaryKey] as? Bool {
                
                return boolValue
            }
        }
        
        return false
    }
}

extension UILabel {
    
    func requiredHeightForTheCurrentText() -> CGFloat {
        
        let label: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = self.font
        label.text = self.text
        
        label.sizeToFit()
        
        return label.bounds.height
    }
}

class TBUIButtonActionButton: UIButton {
    
    convenience init() {
        
        self.init(frame: CGRect.zero)
        
        self.initialiseButton()
    }
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        self.initialiseButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
        self.initialiseButton()
    }
    
    private func initialiseButton() {
        
        self.setTitleColor(UIColor.payQwiqButtons(), for: UIControlState.normal)
        self.setTitleColor(UIColor.payQwiqButtons(), for: UIControlState.highlighted)
        self.setTitleColor(UIColor.payQwiqButtons(), for: UIControlState.disabled)
        
        self.titleLabel?.textColor = UIColor.payQwiqButtons()
        self.backgroundColor = UIColor.payQwiqActionButton()
        
        self.titleLabel?.textColor
    }
}
class TBUIButtonActiveButton: UIButton {
    
    convenience init() {
        
        self.init(frame: CGRect.zero)
        
        self.initialiseButton()
    }
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        self.initialiseButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
        self.initialiseButton()
    }
    
    private func initialiseButton() {
        
        self.setTitleColor(UIColor.payQwiqButtons(), for: UIControlState.normal)
        self.setTitleColor(UIColor.payQwiqButtons(), for: UIControlState.highlighted)
        self.setTitleColor(UIColor.payQwiqButtons(), for: UIControlState.disabled)
        
        self.titleLabel?.font = UIFont.buttons()
        
        self.backgroundColor = UIColor.payQwiqActiveButton()
    }
}
class TBUIButtonIconBackground: UIButton {
    
    convenience init() {
        
        self.init(frame: CGRect.zero)
        
        self.setupButton()
    }
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        self.setupButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
        self.setupButton()
    }
    
    private func setupButton() {
        
        self.titleLabel?.font = UIFont.buttons()
        self.titleLabel?.textColor = UIColor.payQwiqButtons()
        self.backgroundColor = UIColor.payQwiqButtonIconBackground()
    }
}
class TBUIButtonLinkButton: UIButton {
    
    convenience init() {
        
        self.init(frame: CGRect.zero)
        
        self.setupButton()
    }
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        self.setupButton()
    }
    
    required internal init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
        self.setupButton()
    }
    
    private func setupButton() {
        
        self.backgroundColor = UIColor.payQwiqLinkButtonBackground()
        
        let attributes = [NSFontAttributeName: UIFont.linkButton(), NSForegroundColorAttributeName: UIColor.payQwiqMainTitle(), NSUnderlineStyleAttributeName: NSUnderlineStyle.styleThick.rawValue] as [String : Any]

        self.setTitleColor(UIColor.payQwiqMainTitle(), for: .normal)

        if let buttonLabelText = self.titleLabel!.text {
            let attributedText = NSMutableAttributedString(string:buttonLabelText, attributes: attributes)
            self.setAttributedTitle(attributedText, for: .normal)
        }
        
    }
}
class TBUIButtonRounded: UIButton {
    
    // MARK: - Public properties
    
    var buttonIndex = -1
    private let buttonSize = CGSize(width: 80, height: 40)
    
    // MARK: - Private properties
    
    private let buttonCornerRadius = CGFloat(5)
    
    private var buttonSelectBackgroundColor = UIColor.payQwiqRoundedButtonSelectedBackgroundColor()
    private var buttonDeselectBackgroundColor = UIColor.payQwiqRoundedButtonDeselectedBackgroundColor()
    private var buttonSelectTitleColor = UIColor.white
    private var buttonDeselectTitleColor = UIColor.white
    
    convenience init() {
        
        self.init(frame: CGRect.zero)
        
        self.customizeButton()
    }
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        self.customizeButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
        self.customizeButton()
    }
    
    private func customizeButton() {
        
        self.layer.cornerRadius = self.buttonCornerRadius
        self.layer.masksToBounds = true
        
        self.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        
        self.backgroundColor = self.buttonDeselectBackgroundColor
        self.setTitleColor(self.buttonDeselectTitleColor, for: UIControlState.normal)
    }
    
    func selectButton() {
        
        self.backgroundColor = self.buttonSelectBackgroundColor
        self.setTitleColor(self.buttonSelectTitleColor, for: UIControlState.normal)
    }
    
    func deselectButton() {
        
        self.backgroundColor = self.buttonDeselectBackgroundColor
        self.setTitleColor(self.buttonDeselectTitleColor, for: UIControlState.normal)
    }
}
class TBUIButtonSecondaryButton: UIButton {
    
    convenience init() {
        
        self.init(frame: CGRect.zero)
        
        self.customizeButton()
    }
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        self.customizeButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
        self.customizeButton()
    }
    
    private func customizeButton() {
        
        self.setTitleColor(UIColor.payQwiqMainTitle(), for: UIControlState.normal)
//        self.titleLabel?.font = UIFont.otherMessage()
        self.backgroundColor = UIColor.payQwiqSecondaryButton()
    }
}
class TBUIButtonWhiteBorderButton: UIButton {
    
    
    // MARK: - Private properties
    
    convenience init() {
        
        self.init(frame: CGRect.zero)
        setupButton()
    }
    
    // MARK: - Initializers
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        setupButton()

    }
    
    required internal init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        setupButton()

    }
    
    // MARK: - Private methods
    
    private func setupButton() {
        
        self.backgroundColor = UIColor.payQwiqWhiteLinkButtonControl()
        
        self.titleLabel?.textAlignment = NSTextAlignment.center
        self.setTitleColor(UIColor.payQwiqWhiteLinkButton(), for: UIControlState.normal)
//        self.titleLabel?.font = UIFont(name: FontNames.TescoRegular, size:  CGFloat(17.0))!
        
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 1.0
        
        self.contentEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
    }
}
class TBUIButtonWhiteLinkButton: UIButton {
    

    
    // MARK: - Private properties
    
    private var fontSize = CGFloat(0)
    
    // MARK: - Initializers
    
    convenience init() {
        self.init(frame: CGRect.zero)
        setupButton()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupButton()
    }
    
    required internal init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupButton()
    }
    
    // MARK: - Private methods
    
    private func setupButton() {
        
        self.backgroundColor = UIColor.payQwiqWhiteLinkButtonControl()
        
        self.titleLabel?.textAlignment = NSTextAlignment.center
        
        let attributes = [NSFontAttributeName: UIFont(name: FontNames.TescoRegular, size: self.fontSize)!, NSForegroundColorAttributeName: UIColor.payQwiqWhiteLinkButton(), NSUnderlineStyleAttributeName: NSUnderlineStyle.styleThick.rawValue] as [String : Any]
        
        if let buttonLabelText = self.titleLabel!.text {
            
            let attributedText = NSMutableAttributedString(string:buttonLabelText, attributes: attributes)
            self.setAttributedTitle(attributedText, for: .normal)
        }
        
        self.contentEdgeInsets = UIEdgeInsets(top: 15, left: 0, bottom: 10, right: 0)
    }
}
class TBUIRoundedLightBlueButton: UIButton {
    
    
    convenience init() {
        self.init(frame: CGRect.zero)
        setupButton()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupButton()
    }
    
    required internal init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupButton()
    }
    private func setupButton() {
        
//        self.titleLabel?.font = UIFont.registrationJourneyContinueButton()
        
        self.backgroundColor = UIColor.payQwiqLightBlue()
        
        self.setTitleColor(UIColor.white, for: .normal)
        
        self.layer.cornerRadius = CGFloat(5)
    }
}

class TBUILabel: UILabel {
    
    var fontSize = CGFloat(0)
    
    convenience init() {
        self.init(frame: CGRect.zero)
        
        self.initialiseLabel()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.initialiseLabel()
    }
    
    required internal init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.initialiseLabel()
    }
    
    private func initialiseLabel() {
        
        self.font = UIFont(name: FontNames.TescoRegular, size: self.font.pointSize)
    }
    
    func showLabel(fontSize: CGFloat) {
        
        self.font = UIFont(name: FontNames.TescoRegular, size: fontSize)
    }
    
    func hideLabel() {
        
        self.font = UIFont(name: FontNames.TescoRegular, size: 0.0)
    }
}

class TBUILabelBold: UILabel {
    
    convenience init() {
        self.init(frame: CGRect.zero)
        
        self.initialiseLabel()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.initialiseLabel()
    }
    
    required internal init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.initialiseLabel()
    }
    
    private func initialiseLabel() {
        
        self.font = UIFont.boldSystemFont(ofSize: self.font.pointSize)
    }
}

class TBUILabelMainTitle: UILabel {
    
    convenience init() {
        
        self.init(frame: CGRect.zero)
        
        self.initialiseLabel()
    }
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        self.initialiseLabel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
        self.initialiseLabel()
    }
    
    private func initialiseLabel() {
        
        self.font = UIFont.mainTitle()
        self.textColor = UIColor.payQwiqMainTitle()
    }
}

class TBUILabelOtherMessage: UILabel {
    
    convenience init() {
        
        self.init(frame: CGRect.zero)
        self.configureLabel()
    }
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        self.configureLabel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
        self.configureLabel()
    }
    
    private func configureLabel() {
        
        self.font = UIFont.systemFont(ofSize: self.font.pointSize)
        self.textColor = UIColor.black
    }
}

class TBUILabelPrimaryMessage: UILabel {
    
    convenience init() {
        
        self.init(frame: CGRect.zero)
        self.configureLabel()
    }
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        self.configureLabel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
        self.configureLabel()
    }
    
    private func configureLabel() {
        
        self.font = UIFont.primaryMessage()
        self.textColor = UIColor.payQwiqPrimaryMessage()
    }
}

class TBUILabelSecondaryMessage: UILabel {
    
    convenience init() {
        
        self.init(frame: CGRect.zero)
        
        self.initialiseLabel()
    }
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        self.initialiseLabel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
        self.initialiseLabel()
    }
    
    private func initialiseLabel() {
        
        self.font = UIFont.systemFont(ofSize: self.font.pointSize, weight: UIFontWeightLight)
        
        self.textColor = UIColor.payQwiqSecondaryMessage()
    }
}

let rect = CGRect(x: 0, y: 0, width: 100, height: 30)

//DESIGN TEST AREA

var actionButton = TBUIButtonActionButton(frame: rect)
actionButton.setTitle("PayQwiq", for: .normal)

var roundedButton = TBUIButtonRounded(frame: rect)
roundedButton.setTitle("Senka", for: .normal)

var whiteLinkButton = TBUIButtonWhiteLinkButton(frame: rect)
whiteLinkButton.setTitle("PayQwiq", for: .normal)

var whiteBorderButton = TBUIButtonWhiteBorderButton(frame: rect)
whiteBorderButton.setTitle("PayQwiq", for: .normal)

var roundedLightBlueButton = TBUIRoundedLightBlueButton(frame: rect)
roundedLightBlueButton.setTitle("Oli", for: .normal)


var secondaryButton = TBUIButtonSecondaryButton(frame: rect)
secondaryButton.setTitle("PayQwiq", for: .normal)

var linkButton = TBUIButtonLinkButton(frame: rect)
linkButton.setTitle("PayQwiq", for: .normal)

var activeButton = TBUIButtonActiveButton(frame: rect)
activeButton.setTitle("PayQwiq", for: .normal)

var label = TBUILabel(frame: rect)
label.text = "PayQwiq"

var boldLabel = TBUILabelBold(frame: rect)
boldLabel.text = "PayQwiq"

var mainTitle = TBUILabelMainTitle(frame: rect)
mainTitle.text = "PayQwiq"

var otherMessage = TBUILabelOtherMessage(frame: rect)
otherMessage.text = "PayQwiq"

var secondaryMessage = TBUILabelSecondaryMessage(frame: rect)
secondaryMessage.text = "PayQwiq"

