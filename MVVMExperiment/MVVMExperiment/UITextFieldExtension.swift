//
//  UITextFieldExtension.swift
//  MVVMExperiment
//
//  Created by Tim on 25/04/2017.
//  Copyright © 2017 Tesco Bank. All rights reserved.
//

import Foundation
import UIKit


extension UITextField {
    
    func configureTextField(textFieldViewModel: TextFieldViewModel) {
        
        self.placeholder = textFieldViewModel.placholderText
        self.keyboardType = textFieldViewModel.keyboardType
        self.isSecureTextEntry = textFieldViewModel.isSecureTextEntry
        self.isHidden = textFieldViewModel.isHidden
       
        self.resignFirstResponder()
    }
}
