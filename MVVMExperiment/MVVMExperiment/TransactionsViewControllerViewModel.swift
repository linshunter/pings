//
//  TransactionsViewControllerViewModel.swift
//  MVVMExperiment
//
//  Created by Tim on 25/04/2017.
//  Copyright © 2017 Tesco Bank. All rights reserved.
//

import UIKit

protocol TransactionsViewControllerViewModelDelegate {
    
    func label(populateLabelWith viewModel: LabelViewModel)
}

protocol TransactionsViewControllerViewModelProtocol {
    
    func viewReady()
}

class TransactionsViewControllerViewModel: TransactionsViewControllerViewModelProtocol {
    
    private var user = User()
    private (set) var transactions = Transactions()
    
    var delegate: TransactionsViewControllerViewModelDelegate?
    
    func viewReady() {
     
        self.populateName()
    }
    
    private func populateName() {
        
        let userName = "Hi \(self.user.firstName) \(self.user.lastName)"
        
        let viewModel = LabelViewModel(text: userName)
        
        self.delegate?.label(populateLabelWith: viewModel)
    }
}
