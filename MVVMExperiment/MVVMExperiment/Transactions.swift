//
//  Transactions.swift
//  MVVMExperiment
//
//  Created by Tim on 25/04/2017.
//  Copyright © 2017 Tesco Bank. All rights reserved.
//

import UIKit

class Transactions: NSObject {
    
    var transactions = [Transaction]()
    
    override init() {
        
        for _ in 1...100 {
            
            let transaction = Transaction()
            
            self.transactions.append(transaction)
        }
    }
}


class Transaction {
    
    var amount = arc4random_uniform(100)
    var date = NSDate(timeIntervalSince1970: TimeInterval(arc4random_uniform(1493124546)))
}
