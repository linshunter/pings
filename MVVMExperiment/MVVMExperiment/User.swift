//
//  User.swift
//  MVVMExperiment
//
//  Created by Tim on 25/04/2017.
//  Copyright © 2017 Tesco Bank. All rights reserved.
//

import UIKit

class User {
    
    var firstName = "Danny"
    var lastName = "Barnes"
}

class Clubcard {
    
    var clubcardNumber = "12345678901234567890"
    var isValid = true
}
