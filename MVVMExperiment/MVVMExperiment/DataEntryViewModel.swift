//
//  DataEntryViewModel.swift
//  MVVMExperiment
//
//  Created by Tim on 25/04/2017.
//  Copyright © 2017 Tesco Bank. All rights reserved.
//

import UIKit

protocol DataEntryViewModelDelegate {
    
    func textField(configureFirstTextFieldWith textFieldViewModel: TextFieldViewModel)
    
    func textField(configureSecondTextFieldWith textFieldViewModel: TextFieldViewModel)
    
    func textField(configureThirdTextFieldWith textFieldViewModel: TextFieldViewModel)
    
    func textField(configureFourthTextFieldWith textFieldViewModel: TextFieldViewModel)
    
    func textField(configureFifthTextFieldWith textFieldViewModel: TextFieldViewModel)
}

protocol DataEntryViewModelProtocol {
    
    func viewReady()
    
    func switchValueChanged(switchOn: Bool)
    
    func secondSwitchValueChanged(switchOn: Bool)
}

class DataEntryViewModel: DataEntryViewModelProtocol {
    
    var delegate: DataEntryViewModelDelegate?
    
    func viewReady() {
        
        self.delegate?.textField(configureFirstTextFieldWith: TextFieldViewModel())
        self.delegate?.textField(configureSecondTextFieldWith: TextFieldViewModel())
        self.delegate?.textField(configureThirdTextFieldWith: TextFieldViewModel())
        self.delegate?.textField(configureFourthTextFieldWith: TextFieldViewModel())
        self.delegate?.textField(configureFifthTextFieldWith: PasswordTextFieldViewModel())
    }
    
    func switchValueChanged(switchOn: Bool) {
        
        let viewModel = PasswordTextFieldViewModel()
        
        viewModel.isHidden = !switchOn
        
        self.delegate?.textField(configureFifthTextFieldWith: viewModel)
    }
    
    func secondSwitchValueChanged(switchOn: Bool) {
        
        var viewModel = TextFieldViewModel()
        
        if !switchOn {
            
            viewModel = NumericTextFieldViewModel()
        }
        
        self.delegate?.textField(configureFourthTextFieldWith: viewModel)
    }
}
