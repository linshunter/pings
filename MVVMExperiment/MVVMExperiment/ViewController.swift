//
//  ViewController.swift
//  MVVMExperiment
//
//  Created by Tim on 25/04/2017.
//  Copyright © 2017 Tesco Bank. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var firstTextField: UITextField!
    
    @IBOutlet weak var secondTextField: UITextField!
    
    @IBOutlet weak var thirdTextField: UITextField!
    
    @IBOutlet weak var fourthTextField: UITextField!

    @IBOutlet weak var fifthTextField: UITextField!
    
    var viewModel = DataEntryViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewModel.delegate = self
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.viewModel.viewReady()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func switchValueChanged(_ sender: UISwitch) {
        
        self.viewModel.switchValueChanged(switchOn: sender.isOn)
    }
    
    
    @IBAction func secondSwitchValueChanged(_ sender: UISwitch) {
        
         self.viewModel.secondSwitchValueChanged(switchOn: sender.isOn)
    }

    @IBAction func myUnwindAction(unwindSegue: UIStoryboardSegue) {
        
        
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
}

extension ViewController: DataEntryViewModelDelegate {
    
    func textField(configureFirstTextFieldWith textFieldViewModel: TextFieldViewModel) {
        
        self.firstTextField.configureTextField(textFieldViewModel: textFieldViewModel)
    }
    
    func textField(configureSecondTextFieldWith textFieldViewModel: TextFieldViewModel) {
        
        self.secondTextField.configureTextField(textFieldViewModel: textFieldViewModel)
    }
    
    func textField(configureThirdTextFieldWith textFieldViewModel: TextFieldViewModel) {
     
        self.thirdTextField.configureTextField(textFieldViewModel: textFieldViewModel)
    }
    
    func textField(configureFourthTextFieldWith textFieldViewModel: TextFieldViewModel) {
        
        self.fourthTextField.configureTextField(textFieldViewModel: textFieldViewModel)
    }

    func textField(configureFifthTextFieldWith textFieldViewModel: TextFieldViewModel) {
        
        self.fifthTextField.configureTextField(textFieldViewModel: textFieldViewModel)
    }
}


