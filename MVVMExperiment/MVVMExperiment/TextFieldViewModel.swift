//
//  TextFieldViewModel.swift
//  MVVMExperiment
//
//  Created by Tim on 25/04/2017.
//  Copyright © 2017 Tesco Bank. All rights reserved.
//

import UIKit

class TextFieldViewModel: NSObject {
    
    var placholderText = "Enter text"
    var keyboardType = UIKeyboardType.default
    var isSecureTextEntry = false
    var isHidden = false
}

class PasswordTextFieldViewModel: TextFieldViewModel {
    
    override init() {
        
        super.init()
        
        super.placholderText = "Enter Password"
        super.isSecureTextEntry = true
    }
}

class NumericTextFieldViewModel: TextFieldViewModel {
    
    override init() {
        
        super.init()
        
        super.placholderText = "Enter Number"
        super.isSecureTextEntry = false
        super.keyboardType = UIKeyboardType.numberPad
    }
}
