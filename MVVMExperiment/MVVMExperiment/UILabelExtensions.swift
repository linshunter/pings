//
//  UILabelExtensions.swift
//  MVVMExperiment
//
//  Created by Tim on 27/04/2017.
//  Copyright © 2017 Tesco Bank. All rights reserved.
//

import Foundation
import UIKit


extension UILabel {
    
    func configureLabel(withViewModel viewModel: LabelViewModel) {
        
        self.text = viewModel.text
    }
}
