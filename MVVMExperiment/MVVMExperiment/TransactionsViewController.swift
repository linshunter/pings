//
//  TransactionsViewController.swift
//  MVVMExperiment
//
//  Created by Tim on 27/04/2017.
//  Copyright © 2017 Tesco Bank. All rights reserved.
//

import UIKit

class TransactionsViewController: UIViewController {

    @IBOutlet weak var label: UILabel!
    
    var viewModel = TransactionsViewControllerViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.viewModel.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.viewModel.viewReady()
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension TransactionsViewController: TransactionsViewControllerViewModelDelegate {
    
    func label(populateLabelWith viewModel: LabelViewModel) {
        
        self.label.configureLabel(withViewModel: viewModel)
    }
    
}
