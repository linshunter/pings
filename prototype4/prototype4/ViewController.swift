//
//  ViewController.swift
//  prototype4
//
//  Created by Gaurav Sharma on 26/05/2017.
//  Copyright © 2017 tescopay. All rights reserved.
//

import UIKit
import Hero	

class ViewController: UIViewController {

    @IBOutlet weak var rect: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        rect.heroModifiers = [.rotate(x:30), .scale(2)]
        
        self.navigationController?.heroNavigationAnimationType = .none

        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

