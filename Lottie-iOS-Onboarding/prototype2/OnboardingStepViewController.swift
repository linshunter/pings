//
//  ViewController.swift
//  prototype2
//
//  Created by Gaurav Sharma on 16/02/2017.
//  Copyright © 2017 PayQwiq. All rights reserved.
//

import UIKit

class OnboardingStepViewController: UIViewController {

    @IBOutlet weak var animationPlaceholderView: UIView!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var headerLabel: UILabel!
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var parentPageViewController: OnboardingPageViewController!

    @IBOutlet weak var button: UIButton!
    
    var animationView: LOTAnimationView!
    
    var page = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch (page) {
        case 0:
            descriptionLabel.text = "Pay in Tesco with your phone"
            pageControl.currentPage = page
            animationView = LOTAnimationView.animationNamed("IconTransitions")
            button.setTitle("Next", for: .normal)
            button.setTitleColor(UIColor.white, for: .normal)
            button.backgroundColor = UIColor(red: 0, green: 193/255, blue: 240/255, alpha: 1)
        case 1:
            descriptionLabel.text = "Collect Clubcard points automatically"
            pageControl.currentPage = page
            animationView = LOTAnimationView.animationNamed("TwitterHeart")
            button.setTitle("Next", for: .normal)
            button.setTitleColor(UIColor.white, for: .normal)
            button.backgroundColor = UIColor(red: 0, green: 193/255, blue: 240/255, alpha: 1)
        case 2:
            descriptionLabel.text = "Pay up to £250, higher than contactless"
            pageControl.currentPage = page
            animationView = LOTAnimationView.animationNamed("HamburgerArrow")
            button.setTitle("Next", for: .normal)
            button.setTitleColor(UIColor.white, for: .normal)
            button.backgroundColor = UIColor(red: 0, green: 193/255, blue: 240/255, alpha: 1)
        case 3:
            descriptionLabel.text = "Track your spending"
            pageControl.currentPage = page
            animationView = LOTAnimationView.animationNamed("PinJump")
            button.setTitle("Get started", for: .normal)
        default:
            print ("Page does not exist")
        }
        
        animationView.contentMode = .scaleAspectFit

        playAnimation()

        button.layer.cornerRadius = 6
        button.clipsToBounds = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(playAnimation))
        animationView.addGestureRecognizer(tapGesture)
    }
    
    override func viewDidLayoutSubviews() {
        animationView.frame = CGRect(x: 0, y: 0,
                                     width: animationPlaceholderView.frame.width,
                                     height: animationPlaceholderView.frame.height)
        
        animationPlaceholderView.addSubview(animationView!)
    }
    
    func playAnimation() {
        animationView.animationProgress = 0
        animationView?.play(completion: { (finished) in

        })
    }
    
    @IBAction func buttonTouchUp(_ sender: Any) {
        parentPageViewController.nextButtonPressed(currentPage:page)
    }
}
