//
//  TutorialPageViewController.swift
//  prototype2
//
//  Created by Gaurav Sharma on 17/02/2017.
//  Copyright © 2017 PayQwiq. All rights reserved.
//

import UIKit

class OnboardingPageViewController: UIPageViewController {
    
    let pageCount = 4

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setViewControllers([stepForPage(page: 0)], direction: .forward, animated: false, completion: nil)

        dataSource = self
        
        view.backgroundColor = UIColor(red: 0, green: 178/255, blue: 234/255, alpha: 1)
    }

    func stepForPage(page: Int) -> OnboardingStepViewController {
        
        let stepViewController = storyboard!.instantiateViewController(withIdentifier: "OnboardingStepViewController") as! OnboardingStepViewController
        
        stepViewController.page = page
        stepViewController.parentPageViewController = self //todo: ensure no reference cycle

        return stepViewController
    }
    
    func nextButtonPressed (currentPage: Int) {
        
        if currentPage < pageCount - 1 {
            let nextPage = stepForPage(page: currentPage + 1)
            setViewControllers([nextPage], direction: .forward, animated: true, completion: nil)
        }
    }
}

extension OnboardingPageViewController : UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        return moveToNextPage(viewController: viewController)
    }

    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        return moveToPreviousPage(viewController: viewController)
    }
    
    func moveToNextPage (viewController: UIViewController) -> UIViewController? {
        if let currentStepController = viewController as? OnboardingStepViewController {
            if currentStepController.page < pageCount - 1 {
                return stepForPage(page: currentStepController.page + 1)
            }
        }
        return nil
    }
    
    func moveToPreviousPage (viewController: UIViewController) -> UIViewController? {
        if let currentStepController = viewController as? OnboardingStepViewController {
            if currentStepController.page > 0 {
                return stepForPage(page: currentStepController.page - 1)
            }
        }
        return nil
    }
}
