//
//  ColoursCollectionViewController.swift
//  PQPalette
//
//  Created by Gaurav Sharma on 03/05/2017.
//  Copyright © 2017 Tesco. All rights reserved.
//

import UIKit

class ColorsViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var colors = [Color]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        colors.append(Color("Tesco Blue", UIColor.tescoBlue()))
        colors.append(Color("Prussian Blue", UIColor.prussianBlue()))
        
        colors.append(Color("Mine shaft", UIColor.mineShaft()))
        colors.append(Color("Dove grey", UIColor.doveGrey()))

        colors.append(Color("Silver", UIColor.silver()))
        colors.append(Color("Mercury", UIColor.mercury()))
        
        colors.append(Color("Tesco Red", UIColor.tescoRed()))
        colors.append(Color("Persian Red", UIColor.persianRed()))
        
        colors.append(Color("Lochmara", UIColor.lochmara()))
        colors.append(Color("Apple", UIColor.apple()))
        
        colors.append(Color("Black", UIColor.black))
        colors.append(Color("White", UIColor.white))
        
        colors.append(Color("Bali Hai", UIColor.baliHai()))
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return colors.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ColorViewCell", for: indexPath) as! ColorViewCell
    
        let color = colors[indexPath.item]
        
        cell.colorTitleLabel.text = color.name
        cell.colorView.backgroundColor = color.color
        cell.colorHexLabel.text = "#" + String(describing: color.color.toHex()!)
        
        let red = Int((color.color.cgColor.components?[0])! * 255)
        let green =  Int((color.color.cgColor.components?[1])! * 255)
        let blue = Int((color.color.cgColor.components?[2])! * 255)
        let alpha = (color.color.cgColor.components?[3])!.round(to: 2)
        
        cell.colorRgbLabel.text = "rgba(\(red), \(green), \(blue), \(alpha))"
    
        return cell
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            let cellWidth = (((self.collectionView?.frame.width)! - 3 * 20) / 2)
            return CGSize(width: cellWidth, height: cellWidth*1.1)
        case .pad:
            let cellWidth = (((self.collectionView?.frame.width)! - 5 * 20) / 4)
            return CGSize(width: cellWidth, height: cellWidth*1.1)
        default:
            return CGSize(width: 100, height: 100)
        }
    }
}

extension CGFloat {
    func round(to places: Int) -> CGFloat {
        let divisor = pow(10.0, CGFloat(places))
        return Darwin.round(self * divisor) / divisor
    }
    
}
