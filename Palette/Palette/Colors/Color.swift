//
//  ColorModel.swift
//  PQPalette
//
//  Created by Gaurav Sharma on 03/05/2017.
//  Copyright © 2017 Tesco. All rights reserved.
//

import UIKit

class Color {
    
    var name: String!
    var color: UIColor!
    
    init(_ name: String!, _ color: UIColor) {
        self.name = name
        self.color = color
    }
}
