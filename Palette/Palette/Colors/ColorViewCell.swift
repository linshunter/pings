//
//  ColourViewCell.swift
//  PQPalette
//
//  Created by Gaurav Sharma on 03/05/2017.
//  Copyright © 2017 Tesco. All rights reserved.
//

import UIKit

class ColorViewCell: UICollectionViewCell {
    
    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var colorTitleLabel: UILabel!
    @IBOutlet weak var colorHexLabel: UILabel!
    @IBOutlet weak var colorRgbLabel: UILabel!
}
