//
//  UIFontExtensions.swift
//  Payqwiq
//
//  Created by Robert Gacsi on 14/10/2016.
//  Copyright © 2016 Tesco Personal Finance Plc. All rights reserved.
//

import Foundation
import UIKit

struct FontNames {
    
    static let TescoRegular = "Tesco"
    static let TescoBold = "TescoBold"
    static let TescoLight = "TescoLight"
    static let TescoItalic = "TescoItalic"
    static let TescoBlack = "TescoBlack"
    static let TescoBoldItalic = "TescoBoldItalic"
}

extension UIFont {

    class func primaryMessage() -> UIFont {
    
        return UIFont(name: FontNames.TescoBold, size: 17.5)!
    }
    
    class func otherMessage() -> UIFont {
    
        return UIFont(name: FontNames.TescoLight, size: 16)!
    }
    
    class func mainTitle() -> UIFont {
    
        return UIFont(name: FontNames.TescoBold, size: 18.5)!
    }
    
    class func navigationTitle() -> UIFont {
        
        return UIFont(name: FontNames.TescoBold, size: 18)!
    }
    
    class func screenTitle() -> UIFont {
    
        return UIFont(name: FontNames.TescoRegular, size: 17.5)!
    }
    
    class func sectionTitle() -> UIFont {
    
        return UIFont(name: FontNames.TescoRegular, size: 17.5)!
    }
    
    class func tips() -> UIFont {
    
        return UIFont(name: FontNames.TescoRegular, size: 16)!
    }
    
    class func tipLink() -> UIFont {
    
        return UIFont(name: FontNames.TescoBold, size: 16)!
    }
    
    class func actionBarNavigation() -> UIFont {
    
        return UIFont(name: FontNames.TescoLight, size: 19)!
    }
    
    class func actionBarTitle() -> UIFont {
    
        return UIFont(name: FontNames.TescoRegular, size: 19)!
    }
    
    class func buttons() -> UIFont {
    
        return UIFont(name: FontNames.TescoRegular, size: 17.5)!
    }
    
    class func inactiveField() -> UIFont {

        return UIFont(name: FontNames.TescoRegular, size: 16.0)!
    }
    
    class func linkButton() -> UIFont {
    
        return UIFont(name: FontNames.TescoRegular, size: 17.0)!
    }
    
    class func plusButton() -> UIFont {
        
        return UIFont(name: FontNames.TescoRegular, size: 20.0)!
    }
    
    class func plusBoldCardFont() -> UIFont {
        
        return UIFont(name: FontNames.TescoBold, size: 15.0)!
    }
    
    class func plusRegularCardFont() -> UIFont {
        
        return UIFont(name: FontNames.TescoRegular, size: 15.0)!
    }
    
    class func topUpAmountFont() -> UIFont {
        
        return UIFont(name: FontNames.TescoRegular, size: 70.0)!
    }
    
    class func minAndMaxTopUpAmountFont() -> UIFont {
        
        return UIFont(name: FontNames.TescoRegular, size: 14.0)!
    }
    
    class func topUpSubLabelFont() -> UIFont {
        
        return UIFont(name: FontNames.TescoRegular, size: 18.0)!
    }
    
    class func zeroHeightFont() -> UIFont {
        
        return UIFont(name: FontNames.TescoRegular, size: 0.0)!
    }

    class func registrationJourneyContinueButton() -> UIFont {
        
        return UIFont(name: FontNames.TescoRegular, size: 20.0)!
    }
    
    class func termsAndConditionsLabel() -> UIFont {
        
        return UIFont(name: FontNames.TescoRegular, size: 12.0)!
    }
    
    class func aboutYouLabel() -> UIFont {
        
        return UIFont(name: FontNames.TescoRegular, size: 24.0)!
    }
    
    class func aboutYouDescriptionLabel() -> UIFont {
        
        return UIFont(name: FontNames.TescoRegular, size: 16.0)!
    }
    
    class func topUpBalanceDecimalsFont() -> UIFont {
        
        return UIFont(name: FontNames.TescoRegular, size: 22.0)!
    }

    class func topUpBalanceUnitsFont() -> UIFont {
        
        return UIFont(name: FontNames.TescoRegular, size: 30.0)!
    }
}
