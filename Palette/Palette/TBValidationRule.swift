//
//  TBValidationRule.swift
//  TSBTextValidation
//
//  Created by Matt Casey on 14/01/2015.
//  Copyright (c) 2015 Tesco Bank. All rights reserved.
//

import UIKit

struct TBDefinedRegularExpressions {
    
    static let DigitsOnly = "^[0-9]*$"
    static let UKPostcode = "(^(([A-z]{1,2})([0-9Rr][0-9A-z]?)\\s?([0-9])([aAbBdD-hHjJlLnNpP-uUwW-zZ]{2}))$)|(^([B,b][F,f][P,p][O,o]){1}\\s?([0-9]{1,4}$))"
    static let FirstLineAddress = "^(?=.*[\\w])([a-zA-Z\\d,;\\(\\)/\\-&°\\. ]|(?<=\\d)#|(?<=[a-zA-Z])'(?=[a-zA-Z])){1,50}$"
    static let TownCity = "(?=.*[a-zA-Z]{3,})[A-Za-z-,.;'&/() ]*"
    static let AlwaysFails = "(?!)"
    static let LettersAndWhitespaceOnly = "^([\\p{L} ])+$"
    static let DigitsAndWhitespace = "^[0-9 ]+$"
    static let EmailAddress = "^([A-Z0-9a-z._%+-]+@([A-Za-z0-9]{1,63})\\.([0-9A-Za-z]{1,63})(\\.[0-9A-Za-z]*){0,63})$"
    static let PhoneNumber = "^0\\d{9,10}$"
    static let MobileNumber = "^07\\d{9,10}$"
    static let Password = "(?=.{8,})((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])|(?=.*\\d)(?=.*[a-zA-Z])(?=.*[\\W_])|(?=.*[a-z])(?=.*[A-Z])(?=.*[\\W_])).*"
    static let Name = "^([A-Za-z '-.])+$"
    static let LettersNumbersAndWhiteSpace = "^[A-Za-z0-9 ]+$"
    static let FirstNameAndLastName = "^[A-Za-z0-9-_()/\\\\:;,.'&^~]+$"
    static let PayQwiqPlusFirstAndLastName = "^([A-Za-z0-9-. '])+$"
    static let PayQwiqPlusEmailAddress = "^([A-Z0-9a-z._&+-])+@([A-Za-z0-9._&-]{1,})+\\.([A-Za-z0-9._&-])*"
    static let TopUpAmount = "^\\£\\d{1,5}$"
}

class TBValidationRule: NSObject {
    
    // MARK: - Public properties
    
    var regEx: String?
    var shouldRemoveWhitespace: Bool = false
    
    // MARK: - Initializers
    
    init(regEx: String?) {
        
        self.regEx = regEx
    }
    
    override init() {
        
    }
    
    init(regEx: String, shouldRemoveWhitespace: Bool) {
        
        self.regEx = regEx
        self.shouldRemoveWhitespace = shouldRemoveWhitespace
    }
    
    func doesPass(_ valueToTest: String) -> Bool {
        
        if self.shouldRemoveWhitespace {
            
            let valueWithWhitespacesRemoved = valueToTest.replacingOccurrences(of: " ", with: "", options: NSString.CompareOptions.literal, range: nil)
            
            return self.validateExpression(valueWithWhitespacesRemoved)
            
        }
        
        return self.validateExpression(valueToTest)
    }
    
    internal func validateExpression(_ expression: String) -> Bool {
        
        do {
            
            let regularExpression = try NSRegularExpression(pattern: self.regEx!, options: NSRegularExpression.Options.anchorsMatchLines)

            let range = NSRange(location: 0, length: expression.characters.count)
            let matchingOptions = NSRegularExpression.MatchingOptions(rawValue: 0)
            
            let numberOfMatches = regularExpression.numberOfMatches(in: expression, options: matchingOptions, range: range)
            
            return numberOfMatches > 0
            
        } catch {
            
            return false
        }
    }
}
