//
//  Functions.swift
//  Payqwiq
//
//  Created by Lindsey Hunter on 03/08/2016.
//  Copyright © 2016 Tesco Personal Finance Plc. All rights reserved.
//

func debugLog(_ items: Any) {
    
    #if DEBUG || DEBUG_SIMULATOR
        
        debugPrint(items)
        
    #endif
}
