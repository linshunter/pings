//
//  TBTextField.swift
//  TescoBankDigitalWallet
//
//  Created by Iain Frame on 15/12/2014.
//  Copyright (c) 2014 Robots and Pencils Inc. All rights reserved.
//

protocol TBTextFieldProtocol {
    
    func tbTextFieldDidResignFirstResponder(_ tbTextField: TBTextField)
    func tbTextFieldWillGoToNextField(_ nextField: UIResponder?) -> UIResponder?
    func tbTextFieldWillPerformCloseAction(_ tbTextField: TBTextField) -> Bool
}

import UIKit

class TBTextField: UITextField {
    
    var leftImageView: UIImageView = UIImageView()
    var rightImageView: TBImageView = TBImageView()
    var validationRules: [TBValidationRule] = []
    var validateOnTextSet = true
    var tbTextFieldDelegate: TBTextFieldProtocol?
    var rightPaddingValue = CGFloat(0)
    var notificationCenter = NotificationCenter.default
    var appendOnly: Bool = false
    
    fileprivate let toolbarHeight: CGFloat = 40
    fileprivate var nextField: UIResponder?
    fileprivate var previousField: UIResponder?
    fileprivate var closure: (() -> ())?
    
    // MARK: - Initializers
    
    convenience init() {
        
        self.init(frame: CGRect.zero)
        
        self.setupFontAndTextColor()
    }
    
    // MARK: - didMoveToSuperview
    
    override func didMoveToSuperview() {
        
        self.setupFontAndTextColor()
    }
    
    // MARK: - Private methods for init
    
    fileprivate func setupFontAndTextColor() {
        
        self.font = UIFont.inactiveField()
        self.textColor = UIColor.payQwiqInactiveField()
        self.backgroundColor = UIColor.payQwiqInactiveBackground()
        self.layer.borderColor = self.defaultBorderColor().cgColor
        self.layer.borderWidth = 1.0
    }
    
    private func defaultBorderColor() -> UIColor {
        
        return UIColor.payQwiqInactiveBorder()
    }

    func makeFocussedField() {
        
        self.layer.borderColor = UIColor.payQwiqFocusHighlight().cgColor
        self.textColor = UIColor.payQwiqValidatedField()
        self.layer.masksToBounds = true
        self.layer.borderWidth = 1.0
    }
    
    override func didMoveToWindow() {
        
        super.didMoveToWindow()
        
        self.createKeyboardToolbar()
    }
    
    override func becomeFirstResponder() -> Bool {
        
        self.makeFocussedField()
        
        return super.becomeFirstResponder()
    }
    
    func validateField() -> Bool {
        
        var valid: Bool = true
        
        for rule: TBValidationRule in self.validationRules {
            
            if !rule.doesPass(self.text!) {
                
                valid = false
                
                self.showError()
                
                break
            }
        }
        
        if valid {
            
            self.hideError()
        }
        
        return valid
    }
    
    func resignFocus() {
        
        self.setupTextFieldAsUnssigned()
        
        if self.text!.isEmpty {
            
            self.setupTextFieldAsUnssignedWithoutText()
            
        } else {
            
            _ = self.validateField()
        }
        
        self.tbTextFieldDelegate?.tbTextFieldDidResignFirstResponder(self)
    }
    
    fileprivate func setupTextFieldAsUnssigned() {

        self.layer.borderColor = self.defaultBorderColor().cgColor
        self.layer.masksToBounds = true
        self.layer.borderWidth = 1.0
    }
    
    fileprivate func setupTextFieldAsUnssignedWithoutText() {
        
        self.leftImageView.image = nil
        self.layer.borderColor = self.defaultBorderColor().cgColor
        self.textColor = UIColor.payQwiqValidatedField()
    }

    func addPreviousField(_ previousField: UIResponder) {
        
        self.previousField = previousField
    }
    
    func removePreviousField() {
        
        self.previousField = nil
    }
    
    func addNextField(_ nextField: UIResponder, closure: @escaping () -> ()) {
        
        self.closure = closure
        
        self.addNextField(nextField)
    }
    
    func addNextField(_ nextField: UIResponder) {
        
        self.nextField = nextField
    }
    
    func createKeyboardToolbar() {
        
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.superview?.frame.size.width ?? 320, height: self.toolbarHeight))
        
        var barButtons = [UIBarButtonItem]()
        
        barButtons.append(self.createPreviousFieldBarButton())
        
        let previousAndNextButtonSpacer = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.done, target: nil, action: nil)
        barButtons.append(previousAndNextButtonSpacer)
        
        barButtons.append(self.createNextFieldBarButton())
        
        let barButtonSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        barButtons.append(barButtonSpacer)
        
        let xCoordinate = toolbar.frame.origin.x
        let yCoordinate = toolbar.frame.origin.y
        let frame = CGRect(x: xCoordinate, y: yCoordinate, width: toolbarHeight, height: toolbarHeight)
        
        let closeBarButtonItem = self.buildCloseBarButtonItem(frame)
       
        barButtons.append(closeBarButtonItem)
        
        toolbar.items = barButtons
        
        self.inputAccessoryView = toolbar
    }
    
    fileprivate func buildCloseBarButtonItem(_ frame: CGRect) -> UIBarButtonItem {
        
        let closeButton = UIButton(type: UIButtonType.system)
        closeButton.frame = frame
        closeButton.setTitle("Done", for: UIControlState.normal)
        closeButton.addTarget(self, action: #selector(TBTextField.closeKeyboard), for: UIControlEvents.touchUpInside)
        
        let closeBarButtonItem = UIBarButtonItem(customView: closeButton)
        
        return closeBarButtonItem
    }
    
    fileprivate func createPreviousFieldBarButton() -> UIBarButtonItem {
        
        var previousFieldBarButton = UIBarButtonItem()
        
        if self.previousField != nil {
            
            previousFieldBarButton = UIBarButtonItem(image: UIImage(named: "LeftArrow"), style: UIBarButtonItemStyle.done, target: self, action: #selector(TBTextField.goToPreviousField))
            
        } else {
            
            let image =  UIImage(named: "LeftArrow-Disabled")
            let renderedImage =  image?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
            
            previousFieldBarButton = UIBarButtonItem(image: renderedImage, style: UIBarButtonItemStyle.done, target: nil, action: nil)
        }
        
        return previousFieldBarButton
    }
    
    fileprivate func createNextFieldBarButton() -> UIBarButtonItem {
        
        var nextFieldBarButton = UIBarButtonItem()
        
        if self.nextField != nil {
            
            nextFieldBarButton = UIBarButtonItem(image: UIImage(named: "RightArrow"), style: UIBarButtonItemStyle.done, target: self, action: #selector(TBTextField.goToNextField))
            
        } else {
            
            let image =  UIImage(named: "RightArrow-Disabled")
            let renderedImage =  image?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
            
            nextFieldBarButton = UIBarButtonItem(image: renderedImage, style: UIBarButtonItemStyle.done, target: nil, action: nil)
        }
        
        return nextFieldBarButton
    }
    
    func goToPreviousField() {
        
        self.previousField?.becomeFirstResponder()
    }
    
    func goToNextField() {
        
        var nextField = self.nextField
        
        if let delegate = self.tbTextFieldDelegate {
        
            nextField = delegate.tbTextFieldWillGoToNextField(nextField)
        }
        
        nextField?.becomeFirstResponder()
        
        self.closure?()
    }
    
    func closeKeyboard() {

        if let delegate = self.tbTextFieldDelegate {
            
            if !delegate.tbTextFieldWillPerformCloseAction(self) {
                
                return
            }
        }
        _ = self.resignFirstResponder()
    }
    
    override func resignFirstResponder() -> Bool {
        
        self.resignFocus()
        
        return super.resignFirstResponder()
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        
        return CGRect(x: bounds.origin.x + 38, y: bounds.origin.y, width: bounds.size.width - self.rightPaddingValue, height: bounds.size.height)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        
        return self.textRect(forBounds: bounds)
    }
    
    func displayTick() {
        
        let validationTick: UIImage = UIImage(named: "validation_tick")!
        self.leftImageView.image = validationTick
        
        let tickImageSize = CGSize(width: 17.6, height: 13.4)
        
        let xPos = CGFloat(10.0)
        let yPos = (self.bounds.height / 2) - (tickImageSize.height / 2)
        self.leftImageView.frame = CGRect(x: xPos, y: yPos, width: tickImageSize.width, height: tickImageSize.height)
        
        self.addSubview(self.leftImageView)
    }
    
    func resetLeftImage() {
        
        if self.leftImageView.superview != nil {
            
            self.leftImageView.image = nil
            self.leftImageView.removeFromSuperview()
        }
    }
    
    func resetRightImage() {
        
        if self.rightImageView.superview != nil {

            self.rightImageView.imageName = nil
            
            self.rightImageView.removeFromSuperview()
            
            self.layoutIfNeeded()
        }
    }
    
    func showError() {
        
        self.displayCross()
        self.displayErrorBorder()
    }
    
    func hideError() {
        
        self.displayTick()
        self.layer.borderColor = self.defaultBorderColor().cgColor
        self.layer.borderWidth = 1.0
        self.textColor = UIColor.payQwiqValidatedField()
        self.layer.backgroundColor = UIColor.payQwiqInactiveBackground().cgColor
    }
    
    fileprivate func displayErrorBorder() {
        
        self.layer.borderColor = UIColor.payQwiqErrorHighlight().cgColor
        self.layer.borderWidth = 1.0
        self.textColor = UIColor.payQwiqErrorHighlight()
        self.layer.backgroundColor = UIColor.payQwiqErrorBackground().cgColor
    }
    
    fileprivate func displayCross () {
        
        let validationCross: UIImage = UIImage(named: "validation_cross")!
        self.leftImageView.image = validationCross
        
        let xPos = CGFloat(10.0)
        let yPos = (self.bounds.height / 2) - (validationCross.size.height / 2)
        self.leftImageView.frame = CGRect(x: xPos, y: yPos, width: validationCross.size.width, height: validationCross.size.height)
        
        self.addSubview(self.leftImageView)
    }
    
    func displayRightHandImage (_ image: String) {
        
        if self.rightImageView.superview != nil && self.rightImageView.imageName == image {
            
            return
        }
        
        self.displayRightHandImageWithTapGesture(image, tapGesture: nil)
    }
    
    func displayRightHandImageWithTapGesture (_ image: String, tapGesture: UIGestureRecognizer?) {
        
        if self.rightImageView.superview != nil {
            
            self.rightImageView.removeFromSuperview()
            self.layoutIfNeeded()
        }
        
        self.rightImageView = TBImageView()
        self.rightImageView.autoresizingMask = UIViewAutoresizing.flexibleLeftMargin
        self.rightImageView.backgroundColor = UIColor.clear
        self.rightImageView.setImage(imageNamed: image)
        
        let imageSize = CGSize(width: 31.0, height: 25.0)
        let xPos = (self.bounds.width - 9) - (imageSize.width)
        let yPos = (self.bounds.height / 2) - (imageSize.height / 2)
        self.rightImageView.frame = CGRect(x: xPos, y: yPos, width: imageSize.width, height: imageSize.height).integral
        
        self.rightImageView.isUserInteractionEnabled = true
        self.addSubview(self.rightImageView)
        
        if tapGesture != nil {
            
            self.rightImageView.addGestureRecognizer(tapGesture!)
        }
    }
    
    //validation helpers
    func addRule(_ rule: TBValidationRule) {
        
        self.validationRules.append(rule)
    }
    
    func addRules(_ rules: [TBValidationRule]) {
        
        for rule in rules {
            
            self.validationRules.append(rule)
        }
    }
    
    // MARK - Append-only overrides
    
    override var selectedTextRange: UITextRange? {
        
        get {
            
            return super.selectedTextRange
        }
        set (value) {
            
            super.selectedTextRange = value
            
            if self.appendOnly {
                self.text = self.text
            }
        }
    }
    
    override func paste(_ sender: Any?) {
        
        if self.appendOnly {
            
            return
        }
        
        super.paste(sender)
    }
    
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        if self.appendOnly {
            
            if gestureRecognizer.isKind(of: UILongPressGestureRecognizer.self) {
                
                return false
            } else if gestureRecognizer.isKind(of: UITapGestureRecognizer.self) {
                
                if let tapGestureRecognizer = gestureRecognizer as? UITapGestureRecognizer {
                    
                    if tapGestureRecognizer.numberOfTapsRequired == 2 {
                        
                        return false
                    }
                }
            }
        }
        
        return super.gestureRecognizerShouldBegin(gestureRecognizer)
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        
        if self.appendOnly {
            
            if action == #selector(self.paste(_:)) {
                
                return false
            }
        }
        
        return super.canPerformAction(action, withSender: sender)
    }

}
