//
//  Colors.swift
//  PQPalette
//
//  Created by Gaurav Sharma on 03/05/2017.
//  Copyright © 2017 Tesco. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    class func tescoBlue () -> UIColor {
        return UIColor(red: 0, green: 83, blue: 159)
    }
    class func prussianBlue () -> UIColor {
        return UIColor(red: 0, green: 41, blue: 79)
    }
    
    class func mineShaft () -> UIColor {
        return UIColor(red: 51, green: 51, blue: 51)
    }
    class func doveGrey () -> UIColor {
        return UIColor(red: 113, green: 113, blue: 113)
    }
    
    class func silver () -> UIColor {
        return UIColor(red: 204, green: 204, blue: 204)
    }
    class func mercury () -> UIColor {
        return UIColor(red: 229, green: 229, blue: 229)
    }
    
    class func tescoRed () -> UIColor {
        return UIColor(red: 238, green: 28, blue: 46)
    }
    class func persianRed () -> UIColor {
        return UIColor(red: 204, green: 51, blue: 51)
    }
    
    class func lochmara () -> UIColor {
        return UIColor(red: 0, green: 141, blue: 200)
    }
    class func apple () -> UIColor {
        return UIColor(red: 97, green: 164, blue: 69)
    }
    
    class func black () -> UIColor {
        return UIColor(red: 0, green: 0, blue: 0)
    }
    class func white () -> UIColor {
        return UIColor(red: 255, green: 255, blue: 255)
    }
    
    class func baliHai () -> UIColor {
        return UIColor(red: 128, green: 148, blue: 167)
    }
}


