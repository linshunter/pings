//
//  StringExtensions.swift
//  Payqwiq
//
//  Created by Tim on 15/02/2016.
//  Copyright © 2016 Tesco Personal Finance Plc. All rights reserved.
//

import Foundation
import UIKit

//I hate you swift

enum StringErrors: Error {
    
    case serialisationToDictionaryFailed
    case dataEncodingFailed
}

extension String {
    
    func toBool() -> Bool {
        
        switch self {
        case "True", "true", "yes", "1":
            return true
        case "False", "false", "no", "0":
            return false
        default:
            return false
        }
    }
    
    func urlEncode() -> String? {
        
        let characterSet = NSMutableCharacterSet(charactersIn: "_-")
        characterSet.formUnion(with: CharacterSet.alphanumerics)
        
        return self.addingPercentEncoding(withAllowedCharacters: characterSet as CharacterSet)
    }

    func heightForView(_ font: UIFont, width: CGFloat) -> CGFloat {
        
        let label: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = self
        
        label.sizeToFit()
        return label.frame.height
    }
    
    func toDictionary() -> [AnyHashable: Any]? {
        
        guard let data = self.data(using: String.Encoding.utf8) else {
            
            return nil
        }
        
        do {
            
            if let serializedData = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [AnyHashable: Any] {
                
                return serializedData
            }
            
        } catch {
            
        }
        
        return nil
    }
    
    func truncateTo(_ characters: Int) -> String {
        
        if self.characters.count > characters {
            
            return self.substring(to: self.characters.index(self.startIndex, offsetBy: characters))
        }
        
        return self
    }
    
    func trim() -> String {
        
        return  self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    func length() -> Int {
        
        return self.characters.count
    }
}
