//
//  ErrorMessageView.swift
//  Payqwiq
//
//  Created by Adrian Juhasz on 8/30/16.
//  Copyright © 2016 Tesco Personal Finance Plc. All rights reserved.
//

import UIKit

class ErrorMessageView: UIView {
    
    var heightConstraint: NSLayoutConstraint?
    
    var superviewHeightConstraintConstant: CGFloat = 0.0
    
    fileprivate let errorView = UIView()
    fileprivate let errorArrow = UIView()
    
    fileprivate(set) var errorLabel = UILabel()

    fileprivate let topAndBottomOffset = 10.0
    fileprivate let heightOfTriangle: CGFloat = 5.0
    
    fileprivate var isDisplayed = false
    
    fileprivate(set) var errorMessage = ""
    fileprivate(set) var emptyTextErrorMessage = ""
    
    fileprivate(set) var errorArrowBackgroundColor = UIColor.white
    
    convenience init() {
        
        self.init(frame: CGRect.zero)
        
        self.renderView()
    }
    
    // MARK: - didMoveToSuperview
    
    override func didMoveToSuperview() {
        self.renderView()
    }
    
    // MARK: - Public methods
    
    func setText(_ text: String?) {

        self.errorLabel.text = text
        
        if let errorMessage = text {
            self.errorMessage = errorMessage
        }
    }
    
    func animateErrorMessageView(displayed isShow: Bool) {
        
        self.heightConstraint?.priority = 999
        
        UIView.animate(withDuration: 0.3, animations: { [weak self] () -> () in
            
            isShow ? self?.displayedConfiguration() : self?.hiddenConfiguration()
            
            self?.layoutIfNeeded()
        })
    }
    
    func changeErrorArrowBackgroundColor(_ color: UIColor) {
        
        self.errorArrowBackgroundColor = color
    }

    // MARK: - Private methods
    
    fileprivate func renderView() {
        
        self.backgroundColor = UIColor.clear
        
        self.alpha = 0.0
        
        self.autoresizingMask = [UIViewAutoresizing.flexibleWidth]
        
        self.initialiseErrorView()
        self.initialiseErrorArrow()
        self.initialiseErrorLabel()
    }
    
    fileprivate func initialiseErrorView () {
     
        let errorViewFrame = CGRect(x: 0.0, y: 5.0, width: self.bounds.width, height: self.bounds.height)
        
        self.errorView.frame = errorViewFrame
        self.errorView.backgroundColor = UIColor.payQwiqErrorHighlight()
        self.errorView.autoresizingMask = [UIViewAutoresizing.flexibleHeight, UIViewAutoresizing.flexibleWidth]
        self.errorView.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(self.errorView)
    }
    
    fileprivate func initialiseErrorArrow () {

        self.errorArrow.frame = CGRect(x: 0.0, y: 0.0, width: self.bounds.width, height: self.heightOfTriangle)
        self.errorArrow.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleBottomMargin]
        self.errorArrow.backgroundColor = errorView.backgroundColor
        
        self.errorView.addSubview(self.errorArrow)
    }
    
    fileprivate func initialiseErrorLabel () {

        let errorViewFrame = CGRect(x: 5.0, y: 5.0, width: self.bounds.width - 5.0, height: 0.0)
        
        self.errorLabel.frame = errorViewFrame
        self.errorLabel.font = UIFont.otherMessage()
        self.errorLabel.textColor = UIColor.white
        self.errorLabel.numberOfLines = 0
        self.errorLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        self.errorLabel.autoresizingMask = [UIViewAutoresizing.flexibleWidth]
        self.errorView.addSubview(self.errorLabel)
    }
    
    func setEmptyText(_ text: String) {
        
        self.emptyTextErrorMessage = text
    }
    
    func useNormalErrorMessage() {
        
        self.errorLabel.text = self.errorMessage
    }
    
    func useEmptyTextErrorMessage() {
        
        self.errorLabel.text = self.emptyTextErrorMessage.isEmpty ? self.errorMessage: self.emptyTextErrorMessage
    }
        
    fileprivate func hiddenConfiguration() {
        
        self.errorLabel.frame = CGRect(x: self.errorLabel.frame.minX, y: self.errorLabel.frame.minY, width: self.errorLabel.bounds.width, height: 0.0)
        
        self.alpha = 0.0
        
        self.heightConstraint?.constant = 0
        
        self.handleSuperViews(self, mainViewHeightConstant: 0)
    }
    
    fileprivate func displayedConfiguration() {
        
        let labelHeight = max(self.errorLabel.requiredHeightForTheCurrentText(), 40)
        
        self.drawUpArrowOnErrorView()
        
        self.errorLabel.frame = CGRect(x: self.errorLabel.frame.minX, y: self.errorLabel.frame.minY, width: self.errorLabel.bounds.width, height: labelHeight + 5)
        
        self.alpha = 1.0
        
        self.heightConstraint?.constant = labelHeight + 10
        
        self.handleSuperViews(self, mainViewHeightConstant: self.heightConstraint?.constant ?? 0)
    }
    
    fileprivate func handleSuperViews(_ view: UIView, mainViewHeightConstant: CGFloat) {
        
        if let superView = view.superview, superView.self is TextFieldViewProtocol {
            
            self.updateHeightConstraintIfExists(superView, mainViewHeightConstant: mainViewHeightConstant)
            
        } else if let superView = view.superview as UIView? {
            
            self.handleSuperViews(superView, mainViewHeightConstant: mainViewHeightConstant)
        }
    }
    
    fileprivate func updateHeightConstraintIfExists(_ view: UIView, mainViewHeightConstant: CGFloat) {
        
        for constriant in view.constraints {
            
            if Int(round(constriant.constant)) == Int(round(constriant.firstItem.bounds.size.height)) {
                
                if self.superviewHeightConstraintConstant == 0.0 {
                    
                    self.superviewHeightConstraintConstant = constriant.constant
                }
                
                constriant.constant = self.superviewHeightConstraintConstant + mainViewHeightConstant
            }
        }
        
        view.layoutIfNeeded()
    }
    
    fileprivate func handleSuperViewIfScrollView(_ view: UIView, mainViewHeightConstant: CGFloat) {
        
        if let superview = view.superview, superview.isKind(of: UIScrollView.self) {
            
            if let scrollView = view.superview as? UIScrollView! {
                
                let currentContentSize = scrollView.contentSize
                scrollView.contentSize = CGSize(width: currentContentSize.width, height: currentContentSize.height + mainViewHeightConstant)
                
                return
            }
            
            self.handleSuperViewIfScrollView(superview, mainViewHeightConstant: mainViewHeightConstant)
        }
        
        if let superview = view.superview {
            
            self.handleSuperViewIfScrollView(superview, mainViewHeightConstant: mainViewHeightConstant)
        }
        
        self.handleSuperViewIfScrollView(self, mainViewHeightConstant: mainViewHeightConstant)
    }
    
    override func didMoveToWindow() {
        
        self.drawUpArrowOnErrorView()
    }
    
    fileprivate func drawUpArrowOnErrorView () {
        
        self.errorArrow.layer.sublayers = []
        
        self.addLeftShapeLayer()
        
        self.errorView.layoutIfNeeded()
    }
    
    fileprivate func addLeftShapeLayer() {
        
        let leftShapeLayer = CAShapeLayer()
        
        leftShapeLayer.path = self.createPathForTheArrow()
        leftShapeLayer.strokeColor = self.errorArrowBackgroundColor.cgColor
        leftShapeLayer.fillColor = self.errorArrowBackgroundColor.cgColor
        leftShapeLayer.bounds = CGRect(x: 0.0, y: 0.0, width: self.bounds.width, height: self.heightOfTriangle)
        leftShapeLayer.anchorPoint = CGPoint(x: 0.0, y: 0.0)
        leftShapeLayer.position = CGPoint(x: 0.0, y: 0.0)
        
        self.errorArrow.layer.addSublayer(leftShapeLayer)
    }
    
    fileprivate func createPathForTheArrow() -> CGMutablePath {
        
        let halfWidthOfTriangle: CGFloat = 5.0
        let triangleApexXCoordinate: CGFloat = 20.0
        
        let leftPath = CGMutablePath()
        
        leftPath.move(to: CGPoint(x: 0.0, y: 0.0))
        leftPath.move(to: CGPoint(x: self.bounds.width, y: 0.0))
        leftPath.move(to: CGPoint(x: self.bounds.width, y: self.heightOfTriangle))
        leftPath.move(to: CGPoint(x: triangleApexXCoordinate + halfWidthOfTriangle, y: self.heightOfTriangle))
        leftPath.move(to: CGPoint(x: triangleApexXCoordinate, y: 0.0))
        leftPath.move(to: CGPoint(x: triangleApexXCoordinate - halfWidthOfTriangle, y: self.heightOfTriangle))
        leftPath.move(to: CGPoint(x: 0.0, y: self.heightOfTriangle))
        leftPath.move(to: CGPoint(x: 0.0, y: 0.0))
        
        return leftPath
    }
}
