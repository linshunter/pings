//
//  UIColorExtensions.swift
//  Payqwiq
//
//  Created by Robert Danko on 10/12/16.
//  Copyright © 2016 Tesco Personal Finance Plc. All rights reserved.
//

import UIKit

extension UIColor {
    
    // MARK: - init
    
    convenience init(red: Int, green: Int, blue: Int) {
        
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(hex: Int) {
        
        self.init(red: (hex >> 16) & 0xff, green: (hex >> 8) & 0xff, blue: hex & 0xff)
    }
    
    // MARK: - Custom Colors
    
    class func plusBlue() -> UIColor {
        
        return UIColor(red: 0, green: 179, blue: 234)
    }
    
    class func plusBlack() -> UIColor {
        
        return UIColor(red: 26, green: 57, blue: 77)
    }
    
    class func payQwiqSelectedBlue() -> UIColor {
        
        return UIColor(red: 0, green: 155, blue: 221)
    }
    
    class func backgroundColor() -> UIColor {
        
        return UIColor(red: 230, green: 233, blue: 237)
    }
    
    class func payQwiqDarkBlue() -> UIColor {
        
        return UIColor(red: 5, green: 39, blue: 77)
    }
    
    class func payQwiqBlue() -> UIColor {
        
        return UIColor(red: 0, green: 176, blue: 230)
    }
    
    class func payQwiqLightBlueButton() -> UIColor {
        
        return UIColor(red: 64, green: 195, blue: 236)
    }
    
    class func payQwiqYellow() -> UIColor {
        
        return UIColor(red: 255, green: 192, blue: 0)
    }
    
    class func payQwiqBrightGreen() -> UIColor {
        
        return UIColor(red: 113, green: 187, blue: 100)
    }
    
    class func payQwiqCoral() -> UIColor {
        
        return UIColor(red: 252, green: 107, blue: 107)
    }
    
    class func payQwiqBannerRed() -> UIColor {

        return UIColor(red: 255, green: 130, blue: 128)
    }
    
    class func payQwiqBannerBlue() -> UIColor {
        
        return UIColor(red: 26, green: 92, blue: 119)
    }
    
    class func payQwiqSpinnerBlue() -> UIColor {
        
        return UIColor(red: 0, green: 177, blue: 230)
    }
    
    class func payQwiqEmerald() -> UIColor {
        
        return UIColor(red: 28, green: 92, blue:118)
    }
    
    class func payQwiqMidGrey() -> UIColor {
        
        return UIColor(red: 230, green: 233, blue: 237)
    }
    
    class func payQwiqLightGrey() -> UIColor {
        
        return UIColor(red: 232, green: 232, blue: 232)
    }
    
    class func payQwiqTopUpAmountBackgroundGrey() -> UIColor {
    
        return UIColor(red: 238, green: 241, blue: 245)
    }
    
    class func payQwiqDarkPurple() -> UIColor {
        
        return UIColor(red: 125, green: 96, blue: 158)
    }
    
    class func payQwiqPurple() -> UIColor {
        
        return UIColor(red: 151, green: 123, blue: 184)
    }
    
    class func payQwiqLightPurple() -> UIColor {
        
        return UIColor(red: 235, green: 231, blue: 240)
    }
    
    class func payQwiqBrightBlue() -> UIColor {
        
        return UIColor(red: 0, green: 161, blue: 224)
    }
    
    class func payQwiqRoundedButtonSelectedBackgroundColor() -> UIColor {
        
        return UIColor(red: 3.0/255.0, green: 108.0/255.0, blue: 147.0/255.0, alpha: 1.0)
    }
    
    class func payQwiqRoundedButtonDeselectedBackgroundColor() -> UIColor {
        
        return UIColor(red: 0/255.0, green: 179/255.0, blue: 234/255.0, alpha: 1.0)
    }
    
    class func payQwiqLightBlue() -> UIColor {
        
        return UIColor(red: 0, green: 179, blue: 234)
    }
    
    class func payQwiqAddCardDetailsErrorMessageArrowBackgroundColor() -> UIColor {
        
        return UIColor.white
    }
    
    class func legalRequirementsErrorViewBorder() -> UIColor {
        
        return UIColor(red: 240, green: 138, blue: 143)
    }
    
    class func legalRequirementsErrorViewBackground() -> UIColor {
        
        return UIColor(red: 252, green: 248, blue: 249)
    }
    
    // MARK: - Custom font colors
    
    class func payQwiqPrimaryMessage() -> UIColor {
        
        return UIColor(hex: 0x00B1E6)
    }
    
    class func payQwiqSecondaryMessage() -> UIColor {
        
        return UIColor(hex: 0x234162)
    }
    
    class func payQwiqMainTitle() -> UIColor {
        
        return UIColor(hex: 0x05274D)
    }
    
    class func payQwiqSectionTitle() -> UIColor {
        
        return UIColor(hex: 0x00B1E6)
    }
    
    class func payQwiqTips() -> UIColor {
        
        return UIColor(hex: 0x434B54)
    }
    
    class func payQwiqActionBarTitle() -> UIColor {
        
        return UIColor(hex: 0xFFFFFF)
    }
    
    class func payQwiqOtherMessage() -> UIColor {
        
        return UIColor(hex: 0x777777)
    }
    
    class func payQwiqButtons() -> UIColor {
        
        return UIColor(hex: 0xFFFFFF)
    }
    
    class func payQwiqInactiveField() -> UIColor {
        
        return UIColor(red: 199, green: 206, blue: 211)
    }
    
    class func payQwiqValidatedField() -> UIColor {
    
        return UIColor(hex: 0x1A3B50)
    }
    
    class func payQwiqErrorField() -> UIColor {
        
        return UIColor(hex: 0xFD8380)
    }
    
    class func payQwiqWhiteLinkButton() -> UIColor {
        
        return UIColor(hex: 0xFFFFFF)
    }
    
    class func payQwiqSignUpMainTitleColor() -> UIColor {
        
        return UIColor(hex: 0x05274D)
    }
    
    class func payQwiqStandardText() -> UIColor {
        
        return UIColor(red: 115, green: 115, blue: 115)
    }
    
    class func payQwiqMidGreenText() -> UIColor {
        
        return UIColor(red: 124, green: 196, blue: 110)
    }
    
    class func payQwiqDarkGreyText() -> UIColor {
        
        return UIColor(red: 130, green: 130, blue: 130)
    }
    
    // MARK: - Custom field colors
    
    class func payQwiqInactiveBackground() -> UIColor {
        
        return UIColor.white
    }
    
    class func payQwiqInactiveBorder() -> UIColor {
        
        return UIColor(red: 199, green: 206, blue: 211)
    }
    
    class func payQwiqFocusHighlight() -> UIColor {
        
        return self.payQwiqLightBlueButton()
    }
    
    class func payQwiqErrorBackground() -> UIColor {
        
        return UIColor(hex: 0xFFFFFF)
    }
    
    class func payQwiqErrorHighlight() -> UIColor {
        
        return UIColor(hex: 0xFD8380)
    }
    
    class func payQwiqTextFieldPlaceholderTextColor() -> UIColor {
        
        return UIColor(hex: 0x8DA0AC)
    }
    
    // MARK: - Custom control colors
    
    class func payQwiqActionButton() -> UIColor {
        
        return UIColor(hex: 0x00B1E6)
    }
    
    class func payQwiqButtonIconBackground() -> UIColor {
        
        return UIColor(hex: 0x83D0ED)
    }
    
    class func payQwiqActiveButton() -> UIColor {
        
        return UIColor(hex: 0x05274D)
    }
    
    class func payQwiqMainBackground() -> UIColor {
        
        return UIColor(hex: 0xFFFFFF)
    }
    
    class func payQwiqToolTipBackground() -> UIColor {
        
        return UIColor(hex: 0xF1F1F1)
    }
    
    class func payQwiqSecondaryButton() -> UIColor {
        
        return UIColor(hex: 0xC2CAD3)
    }
    
    class func payQwiqDisabledButton() -> UIColor {
        
        return UIColor(hex: 0x64758C)
    }
    
    class func payQwiqLinkButtonBackground() -> UIColor {
        
        return UIColor(hex: 0xFFFFFF)
    }
    
    class func payQwiqWhiteLinkButtonControl() -> UIColor {
        
        return UIColor(hex: 0x0A527F)
    }
    
    class func payQwiqDarkBlueText() -> UIColor {
        
        return UIColor(red: 26.0/255.0, green: 57.0/255.0, blue: 77.0/255.0, alpha: 1.0)
    }
    
    class func payQwiqCardOverlayBackgroundColor() -> UIColor {
        
        return UIColor(red: 252.0/255.0, green: 131.0/255.0, blue: 129.0/255.0, alpha: 1.0)
    }
}
