//
//  Primary1Button.swift
//  Palette
//
//  Created by Gaurav Sharma on 05/05/2017.
//  Copyright © 2017 Tesco. All rights reserved.
//

import UIKit

class Primary1Button: UIButton {
    
    override func didMoveToSuperview() {

        self.layer.cornerRadius = 24
        self.clipsToBounds = true
        
        self.setTitleColor(UIColor.white(), for: .normal)
        
        self.titleLabel?.font =  UIFont.systemFont(ofSize: 15, weight: UIFontWeightSemibold)
        
        self.setBackgroundImage(UIColor.tescoBlue().imageFromColor(), for: .normal)
        self.setBackgroundImage(UIColor.tescoBlue().imageFromColor() , for: .highlighted)
        self.setBackgroundImage(UIColor.mercury().imageFromColor() , for: .disabled)
        
        self.layer.borderWidth = 4
        self.layer.borderColor = UIColor.tescoBlue().cgColor
    }
    
    override var isHighlighted: Bool {
        didSet {
            
            switch isHighlighted {
            case true:
                self.layer.borderColor = UIColor.white().withAlphaComponent(0.6).cgColor
                
                self.titleLabel?.attributedText = NSAttributedString(string: (titleLabel?.text)!, attributes:
                        [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue])
                
            case false:
                self.layer.borderColor = UIColor.tescoBlue().cgColor
                
                self.titleLabel?.attributedText = NSAttributedString(string: (titleLabel?.text)!, attributes: nil)
            }
        }
    }
}
