//
//  Primary1Button.swift
//  Palette
//
//  Created by Gaurav Sharma on 05/05/2017.
//  Copyright © 2017 Tesco. All rights reserved.
//

import UIKit

class TextLink1Button: UIButton {
    
    override func didMoveToSuperview() {
        
        self.setTitleColor(UIColor.tescoBlue(), for: .normal)
        self.titleLabel?.font =  UIFont.systemFont(ofSize: 15, weight: UIFontWeightSemibold)
    }
    
    override var isHighlighted: Bool {
        didSet {
            
            switch isHighlighted {
            case true:
                
                self.titleLabel?.attributedText = NSAttributedString(string: (titleLabel?.text)!, attributes:
                    [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue])
                
            case false:
                self.titleLabel?.attributedText = NSAttributedString(string: (titleLabel?.text)!, attributes: nil)
            }
        }
    }
    
    override var isEnabled: Bool {
        didSet {
            
            switch isEnabled {
            case true:
                self.setTitleColor(UIColor.tescoBlue(), for: .normal)
                
            case false:
                self.setTitleColor(UIColor.mercury(), for: .normal)
            }
        }
    }
}
