//
//  caption3Label.swift
//  Palette
//
//  Created by Gaurav Sharma on 05/05/2017.
//  Copyright © 2017 Tesco. All rights reserved.
//

import UIKit

class Caption3Label: UILabel {

    override func didMoveToSuperview() {

        let caption1Font = UIFont.preferredFont(forTextStyle: .caption1)
        
        self.font = UIFont.systemFont(ofSize: caption1Font.pointSize, weight: UIFontWeightMedium)
        
        self.textColor = UIColor.mineShaft()
    }
}
