//
//  caption3Label.swift
//  Palette
//
//  Created by Gaurav Sharma on 05/05/2017.
//  Copyright © 2017 Tesco. All rights reserved.
//

import UIKit

class Subhead2Label: UILabel {

    override func didMoveToSuperview() {

        let subheadFont = UIFont.preferredFont(forTextStyle: .subheadline)
        
        self.font = UIFont.systemFont(ofSize: subheadFont.pointSize, weight: UIFontWeightSemibold)

        self.textColor = UIColor.mineShaft()
    }
}
