//
//  caption3Label.swift
//  Palette
//
//  Created by Gaurav Sharma on 05/05/2017.
//  Copyright © 2017 Tesco. All rights reserved.
//

import UIKit

class SubheadLabel: UILabel {

    override func didMoveToSuperview() {

        let subheadFont = UIFont.preferredFont(forTextStyle: .subheadline)
        
        self.font = subheadFont
        
        self.textColor = UIColor.mineShaft()
    }
}
