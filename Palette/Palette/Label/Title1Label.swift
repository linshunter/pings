//
//  caption3Label.swift
//  Palette
//
//  Created by Gaurav Sharma on 05/05/2017.
//  Copyright © 2017 Tesco. All rights reserved.
//

import UIKit

class Title1Label: UILabel {
    
    @IBInspectable var textStyle: String? = nil {
        didSet {
//            let font = getStyleForTextStyle(textStyle)
//            self.font = UIFont(name: "TESCOModern-Bold", size: title1Font.pointSize + 16)
            
            self.textColor = UIColor.tescoBlue()
        }
    }
    
}
