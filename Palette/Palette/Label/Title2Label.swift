//
//  caption3Label.swift
//  Palette
//
//  Created by Gaurav Sharma on 05/05/2017.
//  Copyright © 2017 Tesco. All rights reserved.
//

import UIKit

class Title2Label: UILabel {
    
    override func didMoveToSuperview() {

        let title2Font = UIFont.preferredFont(forTextStyle: .title2)
        
        self.font = UIFont(name: "TESCOModern-Bold", size: title2Font.pointSize + 10)
        
        self.textColor = UIColor.tescoBlue()
    }
}
