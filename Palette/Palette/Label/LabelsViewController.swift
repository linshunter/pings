//
//  TextTableViewController.swift
//  PQPalette
//
//  Created by Gaurav Sharma on 03/05/2017.
//  Copyright © 2017 Tesco. All rights reserved.
//

import UIKit

class LabelsViewController: UITableViewController {

    @IBOutlet weak var headline1Label: UILabel!

    @IBOutlet weak var title1Label: UILabel!
    @IBOutlet weak var title2Label: UILabel!
    @IBOutlet weak var title3Label: UILabel!

    @IBOutlet weak var subhead1Label: UILabel!
    @IBOutlet weak var subhead2Label: UILabel!
    @IBOutlet weak var subhead3Label: UILabel!
    
    @IBOutlet weak var body1Label: UILabel!

    @IBOutlet weak var caption1Label: UILabel!
    @IBOutlet weak var caption2Label: UILabel!
    @IBOutlet weak var caption3Label: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
