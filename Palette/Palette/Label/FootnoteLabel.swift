//
//  caption3Label.swift
//  Palette
//
//  Created by Gaurav Sharma on 05/05/2017.
//  Copyright © 2017 Tesco. All rights reserved.
//

import UIKit

class FootnoteLabel: UILabel {

    override func didMoveToSuperview() {

        let footnoteFont = UIFont.preferredFont(forTextStyle: .footnote)
        
        self.font = footnoteFont
        
        self.textColor = UIColor.mineShaft()
    }
}
