//
//  caption3Label.swift
//  Palette
//
//  Created by Gaurav Sharma on 05/05/2017.
//  Copyright © 2017 Tesco. All rights reserved.
//

import UIKit

class BodyLabel: UILabel {

    override func didMoveToSuperview() {

        let bodyFont = UIFont.preferredFont(forTextStyle: .body)
        
        self.font = bodyFont
        
        self.textColor = UIColor.mineShaft()
    }
}
