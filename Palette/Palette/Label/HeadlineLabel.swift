//
//  caption3Label.swift
//  Palette
//
//  Created by Gaurav Sharma on 05/05/2017.
//  Copyright © 2017 Tesco. All rights reserved.
//

import UIKit

class HeadlineLabel: UILabel {

    override func didMoveToSuperview() {

        let headlineFont = UIFont.preferredFont(forTextStyle: .headline)
    
        self.font = headlineFont.withSize(headlineFont.pointSize + 5)
        
        self.textColor = UIColor.mineShaft()
    }
}
