//
//  caption3Label.swift
//  Palette
//
//  Created by Gaurav Sharma on 05/05/2017.
//  Copyright © 2017 Tesco. All rights reserved.
//

import UIKit

class Title4Label: UILabel {

    override func didMoveToSuperview() {

        let title3Font = UIFont.preferredFont(forTextStyle: .title3)
        
        self.font = UIFont(name: "TESCOModern-Regular", size: title3Font.pointSize + 8)
        
        self.textColor = UIColor.mineShaft()
    }
}
