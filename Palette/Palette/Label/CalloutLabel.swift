//
//  caption3Label.swift
//  Palette
//
//  Created by Gaurav Sharma on 05/05/2017.
//  Copyright © 2017 Tesco. All rights reserved.
//

import UIKit

class CalloutLabel: UILabel {

    override func didMoveToSuperview() {

        let calloutFont = UIFont.preferredFont(forTextStyle: .callout)
        
        self.font = calloutFont
        
        self.textColor = UIColor.mineShaft()
    }
}
