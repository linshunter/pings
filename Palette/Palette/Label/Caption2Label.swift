//
//  caption3Label.swift
//  Palette
//
//  Created by Gaurav Sharma on 05/05/2017.
//  Copyright © 2017 Tesco. All rights reserved.
//

import UIKit

class Caption2Label: UILabel {
    
    override func didMoveToSuperview() {

        let caption2Font = UIFont.preferredFont(forTextStyle: .caption2)
        
        self.font = caption2Font
        
        self.textColor = UIColor.mineShaft()
    }
}
