//
//  UILabelExtensions.swift
//  Payqwiq
//
//  Created by Adrian Juhasz on 8/18/16.
//  Copyright © 2016 Tesco Personal Finance Plc. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    
    func requiredHeightForTheCurrentText() -> CGFloat {
        
        let label: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = self.font
        label.text = self.text
        
        label.sizeToFit()
        
        return label.bounds.height
    }
}
