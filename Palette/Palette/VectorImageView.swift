//
//  VectorImageView.swift
//  Payqwiq
//
//  Created by Simon Lawrence on 07/03/2017.
//  Copyright © 2017 Tesco Personal Finance PLC. All rights reserved.
//

import UIKit
import AVFoundation

class VectorImageView: UIView {

    fileprivate var _imageName: String?
    
    @IBInspectable
    var imageName: String? {
        get {
            
            return _imageName
        }
        
        set (newValue) {
            
            _imageName = newValue
            
            self.setNeedsDisplay()
        }
    }
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        
        self.setNeedsDisplay()
    }
    
    override func draw(_ rect: CGRect) {
        
        guard Thread.current.isMainThread, let context = UIGraphicsGetCurrentContext() else {
            
            return
        }
        
        _ = self.drawRect(rect, inContext: context)
    }
    
    func drawRect(_ rect: CGRect, inContext context: CGContext) -> Bool {
        
        let bundle = Bundle(for: type(of: self))
        
        guard let imageName = self._imageName, let fileURL = bundle.url(forResource: imageName, withExtension: "pdf"), let document = CGPDFDocument(fileURL as CFURL) else {
            
            if self._imageName != nil {
                debugLog("\(self._imageName!).pdf could not be loaded from the application bundle.")
            }
            
            super.draw(rect)
            
            return false
        }
        
        guard let page = document.page(at: 1) else {
            
            return false
        }
        
        context.saveGState()
        defer { context.restoreGState() }
        
        self.drawPage(page, inRect: rect, inContext: context)
        
        return true
    }
    
    fileprivate func drawPage(_ page: CGPDFPage, inRect rect: CGRect, inContext context: CGContext) {
        
        let backgroundColor = self.backgroundColor ?? UIColor.clear
        
        let pageRect = page.getBoxRect(.mediaBox)
        
        let fitRect = AVMakeRect(aspectRatio: pageRect.size, insideRect: self.bounds)

        let scale = fitRect.size.width / pageRect.size.width
        
        context.setFillColor(backgroundColor.cgColor)
        context.fill(rect)
        
        context.translateBy(x: 0.0, y: self.bounds.size.height)
        context.translateBy(x: fitRect.origin.x, y: -fitRect.origin.y)
        context.scaleBy(x: 1.0, y: -1.0)
        context.scaleBy(x: scale, y: scale)
        
        context.drawPDFPage(page)
    }
}
