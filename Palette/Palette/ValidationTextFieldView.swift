//
//  ValidationTextFieldView.swift
//  Payqwiq
//
//  Created by Adrian Juhasz on 9/1/16.
//  Copyright © 2016 Tesco Personal Finance Plc. All rights reserved.
//

import UIKit

enum TextFieldEntryType {
    
    case `default`
    case numeric
    case email
    case secure
    case capitalLettersOnly
    case phone
}

@objc protocol ValidationTextFieldViewProtocol {
    
    @objc optional func validationTextFieldViewDidResignFirstResponder(_ validationTextFieldView: ValidationTextFieldView)
    @objc optional func validationTextFieldViewDidReturn(_ validationTextFieldView: ValidationTextFieldView)
    @objc optional func validationTextFieldViewShouldChangeCharactersInRange(_ textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString: String) -> Bool
    @objc optional func validationTextFieldViewWillGoTo(_ validationTextFieldView: ValidationTextFieldView, willGoTo: UIResponder?) -> UIResponder?
    @objc optional func validationTextFieldViewWillPerformCloseAction(_ validationTextFieldView: ValidationTextFieldView) -> Bool
}

protocol TextFieldViewProtocol {
      
}

class ValidationTextFieldView: UIView, TextFieldViewProtocol {
    
    @IBOutlet fileprivate weak var errorMessageViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var tbTextField: TBTextField!
    @IBOutlet weak var errorMessageView: ErrorMessageView!
    
    var validationTextFieldViewDelegate: ValidationTextFieldViewProtocol?
    
    fileprivate(set) var textFieldValidator: BaseTextFieldValidator?
    
    fileprivate(set) var minLength = 1
    fileprivate(set) var maxLength: Int?
    fileprivate(set) var allowedChars: String?
    
    fileprivate(set) var isValid = false
    fileprivate(set) var failureReason: Int?
    
    var enabled: Bool {
        
        get {
            
            return self.tbTextField.isEnabled
        }
        set {
            
            self.tbTextField.isEnabled = newValue
        }
    }
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        self.xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
        self.xibSetup()
    }
    
    fileprivate func xibSetup() {
        
        self.backgroundColor = UIColor.clear
        
        if let contentView = self.loadViewFromNib() {
            
            contentView.frame = bounds
            contentView.backgroundColor = UIColor.clear
            contentView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
            
            self.addSubview(contentView)
        }
        
        self.tbTextField.delegate = self
        self.tbTextField.tbTextFieldDelegate = self
        
        self.errorMessageView.heightConstraint = self.errorMessageViewHeightConstraint
    }
    
    fileprivate func loadViewFromNib() -> UIView? {
        
        let bundle = Bundle(for: ValidationTextFieldView.self)
        let nib = UINib(nibName: String(describing: ValidationTextFieldView.self), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView
        
        return view
    }

    func getPlaceholder() -> String {
        
        return self.tbTextField.placeholder!
    }
    
    func errorMessageViewArrowBackgroundColor() -> UIColor {
        
        return self.errorMessageView.errorArrowBackgroundColor
    }
    
    // MARK: - Setters
    
    func setPrePopulatedText(_ text: String) {
        
        guard let textFieldValidator = self.textFieldValidator, textFieldValidator.validate(text).isValid else {
            
            self.setText("")
            
            return
        }
        
        self.setText(text)
        
        _ = self.validate()
    }
    
    func setPlaceholder(_ text: String) {
        
        self.tbTextField.placeholder = text
        self.tbTextField.attributedPlaceholder = NSAttributedString(string: self.tbTextField.placeholder ?? "", attributes: [NSForegroundColorAttributeName: UIColor.payQwiqTextFieldPlaceholderTextColor()])
    }
    
    func setErrorMessage(_ text: String) {
    
        self.errorMessageView.setText(text)
    }
    
    func setEmptyErrorMessage(_ text: String) {
        
        self.errorMessageView.setEmptyText(text)
    }
    
    func setTextFieldValidator(_ validator: BaseTextFieldValidator) {
    
        self.textFieldValidator = validator
    }
    
    func setMinimumTextLength(_ length: Int) {
        
        self.minLength = length
    }
    
    func setMaximumTextLength(_ length: Int) {
        
        self.maxLength = length
    }
    
    func setAllowedCharacters(_ string: String) {
        
        self.allowedChars = string
    }
    
    func enableEmailTextEntry() {
        
        self.tbTextField.keyboardType = UIKeyboardType.emailAddress
        self.tbTextField.autocapitalizationType = UITextAutocapitalizationType.none
        self.tbTextField.autocorrectionType = UITextAutocorrectionType.no
    }
    
    func enableNumericEntry() {
        
        self.tbTextField.keyboardType = UIKeyboardType.numberPad
    }
    
    func disableAutocorrection() {
        
        self.tbTextField.autocorrectionType = UITextAutocorrectionType.no
        
    }
    func enableSecureTextEntry() {
        
        self.tbTextField.isSecureTextEntry = true
        self.tbTextField.font = nil
        self.tbTextField.font = UIFont.inactiveField()
    }
    
    func disableSecureTextEntry() {
        
        self.tbTextField.isSecureTextEntry = false
        self.tbTextField.font = nil
        self.tbTextField.font = UIFont.inactiveField()
    }
    
    func enableCapitalLettersOnly() {
        
        self.tbTextField.autocapitalizationType = UITextAutocapitalizationType.allCharacters
    }
    
    func enableCapitalLettersForFirstLetterOnly() {
        
        self.tbTextField.autocapitalizationType = UITextAutocapitalizationType.words
    }

    func setText(_ text: String?) {
    
        self.tbTextField.text = text
    }
    
    func changeErrorArrowBackgroundColor(_ color: UIColor) {
        
        self.errorMessageView.changeErrorArrowBackgroundColor(color)
    }
    
    // MARK: - Getters
    
    func text() -> String {
        
        return self.tbTextField.text ?? ""
    }
    
    func errorText() -> String {
    
        return self.errorMessageView.errorMessage
    }
    
    func emptyErrorText() -> String {
        
        if self.errorMessageView.emptyTextErrorMessage == "" {
            
           return self.errorMessageView.errorLabel.text ?? ""
        }
        
        return self.errorMessageView.emptyTextErrorMessage
    }
    
    func placeholderText() -> String {
    
        return self.tbTextField.placeholder ?? ""
    }
    
    func setPlaceholderTextColor(_ color: UIColor) {
        
        self.tbTextField.attributedPlaceholder = NSAttributedString(string: self.tbTextField.placeholder ?? "", attributes: [NSForegroundColorAttributeName: color])
    }
    
    func textColor() -> UIColor {
        
        return self.tbTextField.textColor!
    }
    
    func addPreviousField(_ previousField: UIResponder) {
        
        self.tbTextField.addPreviousField(previousField)
    }
    
    func removePreviousField(_ previousField: UIResponder) {
        
        self.tbTextField.removePreviousField()
    }
    
    func addNextField(_ nextField: UIResponder, closure: @escaping () -> ()) {
        
        self.tbTextField.addNextField(nextField, closure: closure)
    }
    
    func addNextField(_ nextField: UIResponder) {
        
        self.tbTextField.addNextField(nextField)
    }
    
    override func resignFirstResponder() -> Bool {
        
        return self.tbTextField.resignFirstResponder()
    }
    
    override func becomeFirstResponder() -> Bool {
        
        return self.tbTextField.becomeFirstResponder()
    }
    
    override var isFirstResponder: Bool {
        return self.tbTextField.isFirstResponder
    }
    
    func secureTextEntry() -> Bool {
        
        return self.tbTextField.isSecureTextEntry
    }
    
    func makeFocussedField() {
        
        self.tbTextField.makeFocussedField()
    }
    
    func resignFocus() {
        
        self.tbTextField.resignFocus()
    }
    
    func displayRightHandImage(_ image: String) {
        
        self.tbTextField.displayRightHandImage(image)
    }
    
    func resetRightImage() {
        
        self.tbTextField.resetRightImage()
    }

    func setTextFieldEnabled(_ enabled: Bool) {
        
        self.enabled = enabled
    }
    
    func setTextFieldTextColor(_ color: UIColor) {
        
        self.tbTextField.textColor = color
    }
    
    func validate() -> Bool {
        
        self.isValid = false

        guard let textFieldValidator = self.textFieldValidator else {
            
            return true
        }
        
        let validationResult = textFieldValidator.validate(self.text())
        
        self.isValid = validationResult.isValid
        self.failureReason = validationResult.failureReason
        
        self.updateErrorMessageView()
        
        return self.isValid
    }
    
    func isTextFieldValid() -> Bool {
        
        guard let textFieldValidator = self.textFieldValidator else {
            
            return false
        }
        
        return textFieldValidator.validate(self.text()).isValid
    }
    
    func numberOfRules() -> Int {
        
        if let validator = self.textFieldValidator {
            
            return validator.rules().count
        }
        
        return 0
    }
    
    func updateErrorMessageView() {
    
        guard let textFieldValidator = self.textFieldValidator else {
            
            return
        }
        
        if textFieldValidator.validate(self.text()).isValid {
            
            self.errorMessageView.animateErrorMessageView(displayed: false)
            
            self.tbTextField.hideError()
        } else {
            
            self.text().isEmpty ? (self.errorMessageView.errorLabel.text = self.emptyErrorText()) : (self.errorMessageView.errorLabel.text = self.errorText())
            
            self.errorMessageView.animateErrorMessageView(displayed: true)
            
            self.tbTextField.showError()
        }
    }
}

// MARK: - UITextFieldDelegate

extension ValidationTextFieldView: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.validationTextFieldViewDelegate?.validationTextFieldViewDidReturn?(self)
        
        self.endEditing(true)
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if self.textIsTooLong() && self.userIsDeletingText(string, range: range) {
            
            return true
        }
        
        if !self.isValidLength(range) || !self.isAllowed(string) {
            
            return false
        }
        
        if let textFieldDelegate = self.validationTextFieldViewDelegate as ValidationTextFieldViewProtocol! {
            
            if let result = textFieldDelegate.validationTextFieldViewShouldChangeCharactersInRange?(self.tbTextField, shouldChangeCharactersInRange: range, replacementString: string) {
                
                return result
            }
        }
        
        return true
    }
    
    fileprivate func textIsTooLong() -> Bool {
        
        return self.text().length() > (self.maxLength ?? Int.max)
    }
    
    fileprivate func userIsDeletingText(_ replacementString: String, range: NSRange) -> Bool {
        
        return range.length > 0 && replacementString.isEmpty
    }
    
    fileprivate func isAllowed(_ string: String) -> Bool {
        
        if let allowed = self.allowedChars {
            
            let charSet = CharacterSet(charactersIn: allowed).inverted
            let compInSet = string.components(separatedBy: charSet)
            let filter = compInSet.joined(separator: "")
            
            return string == filter
        }
        
        return true
    }
    
    fileprivate func isValidLength(_ range: NSRange) -> Bool {
        
        if let maxNumberOfChars = self.maxLength {
            
            return (tbTextField.text!.characters.count - range.length) < maxNumberOfChars
        }
        
        return true
    }

}

// MARK: - TBTextFieldProtocol methods

extension ValidationTextFieldView : TBTextFieldProtocol {
    
    func tbTextFieldDidResignFirstResponder(_ tbTextField: TBTextField) {
        
        _ = self.validate()
        
        self.validationTextFieldViewDelegate?.validationTextFieldViewDidResignFirstResponder?(self)
    }
    
    func tbTextFieldWillGoToNextField(_ nextField: UIResponder?) -> UIResponder? {
        
        var nextResponder = nextField
        
        if let delegate = self.validationTextFieldViewDelegate {
            
            if let imp = delegate.validationTextFieldViewWillGoTo {
                
                nextResponder = imp(self, nextField)
            }
        }
        
        return nextResponder
    }
    
    func tbTextFieldWillPerformCloseAction(_ tbTextField: TBTextField) -> Bool {
    
        if let delegate = self.validationTextFieldViewDelegate {
            
            if let imp = delegate.validationTextFieldViewWillPerformCloseAction {
                
                return imp(self)
            }
        }
        
        return true
    }
}
