//
//  ValidationConfigurator.swift
//  Payqwiq
//
//  Created by Adrian Juhasz on 9/2/16.
//  Copyright © 2016 Tesco Personal Finance Plc. All rights reserved.
//

typealias TextFieldValidationResult = (isValid: Bool, failureReason: Int?)

class BaseTextFieldValidator {
    
    func rules() -> [TBValidationRule] {
        
        return []
    }
    
    func validate(_ text: String) -> TextFieldValidationResult {

        var isValid = true
        
        for aValidationRule in rules() {
            
            isValid = aValidationRule.doesPass(text)
            
            if !isValid {
                
                break
            }
        }
        
        return (isValid: isValid, failureReason: Optional.none)
    }
}
