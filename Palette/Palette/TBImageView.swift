//
//  TBImageView.swift
//  TescoBankDigitalWallet
//
//  Created by Matt Casey on 20/01/2015.
//  Copyright (c) 2015 Robots and Pencils Inc. All rights reserved.
//

import UIKit

class TBImageView: VectorImageView {
    /*var imageName: String? */
     
    func setImage(imageNamed imageName: String) {
        self.imageName = imageName
    }
}
