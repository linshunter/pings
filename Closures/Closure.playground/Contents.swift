//: Playground - noun: a place where people can play

import UIKit


var basicClosure = { print("Welcome to the Closure Ping") }
basicClosure()

let divide = {(val1: Int, val2: Int) -> Int in
    return val1 / val2
}
let result = divide(200, 20)
print(result)


var shorthand: (String, String) -> String
shorthand = { $1 }
print(shorthand("100", "200"))

func calcDecrement(forDecrement total: Int) -> () -> Int {
    var overallDecrement = 100
    func decrementer() -> Int {
        overallDecrement -= total
        print(overallDecrement)
        return overallDecrement
    }
    return decrementer
}
let decrem = calcDecrement(forDecrement: 18)
decrem()
decrem()


func funcReturnsAClosureThatReturnsString() -> () -> String {
    let result = { () -> String in return
        "So many closures, so little time"
    }
    return result
}

var output = funcReturnsAClosureThatReturnsString()
print(output())


func returnBasicClosure(parOne:String, completion: (name: String) -> String) -> String {
    
    let result = completion(name: parOne)
    
    return "\(parOne) \(result)"
}

print(returnBasicClosure("Yus", completion: { (name) -> String in
    name
}))


func calculate(value: Int, factor: () -> Int) -> Int {
    
    return value * factor()
}

