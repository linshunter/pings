//
//  ViewController.swift
//  ResizeTextTableView
//
//  Created by Tim on 25/08/2017.
//  Copyright © 2017 Tesco Bank. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var table: UITableView!
    
    @IBOutlet weak var label: UILabel!
    
    fileprivate var lastContentOffSet: CGFloat = 0.0
    
    fileprivate var minimumFontSize: CGFloat = 15.0
    fileprivate var maximumFontSize: CGFloat = 40.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.table.delegate = self
        self.table.dataSource = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ViewController: UITableViewDelegate {

    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let newFontSize = self.calculateNewFontSize(currentYPosition: scrollView.contentOffset.y)
        
        if newFontSize >= self.minimumFontSize && newFontSize <= self.maximumFontSize {
            
            self.label.font = self.label.font.withSize(newFontSize)
            
            self.lastContentOffSet = scrollView.contentOffset.y
        }
    }
    
    private func calculateNewFontSize(currentYPosition: CGFloat) -> CGFloat {
        
        var newFontSize = self.maximumFontSize
        
        if currentYPosition > 0 {
            
            let contentOffsetDifference = self.lastContentOffSet - currentYPosition
            
            newFontSize = self.label.font.pointSize + (contentOffsetDifference / 10.0)
        }
        
        return newFontSize
    }
}

extension ViewController: UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 40
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let tableCell = UITableViewCell()
        
        tableCell.backgroundColor = UIColor.blue
        
        return tableCell
    }
}

