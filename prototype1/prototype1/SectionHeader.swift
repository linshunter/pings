//
//  HeaderView.swift
//  prototype1
//
//  Created by Gaurav Sharma on 11/05/2017.
//  Copyright © 2017 tescopay. All rights reserved.
//

import UIKit
import FZAccordionTableView

class SectionHeader: FZAccordionTableViewHeaderView {
    @IBOutlet weak var titleLabel: UILabel!
}
