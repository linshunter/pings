//
//  ViewController.swift
//  prototype1
//
//  Created by Gaurav Sharma on 10/05/2017.
//  Copyright © 2017 tescopay. All rights reserved.
//

import UIKit
import FZAccordionTableView

class ViewController: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        
        let nib = UINib(nibName: "TransactionCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "TransactionCell")
        
        tableView.register(UINib(nibName: "SectionHeader", bundle: nil),
                           forHeaderFooterViewReuseIdentifier: "SectionHeader")
        
        let fzAccordionTableView = tableView as! FZAccordionTableView
        
        fzAccordionTableView.allowMultipleSectionsOpen = false
        
        fzAccordionTableView.initialOpenSections = [0]
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 2:
            return 20
        default:
            return super.tableView(tableView, numberOfRowsInSection: section)
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 2:
            var cell = tableView.dequeueReusableCell(withIdentifier: "TransactionCell")
            
            if cell == nil {
                cell = TransactionCell(style: .default, reuseIdentifier: "TransactionCell")
            }
            
            print("got cell: \(cell!)")

            return cell!
        default:
            return super.tableView(tableView, cellForRowAt: indexPath)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 2:
            return 46
        default:
            return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }
    
    override func tableView(_ tableView: UITableView, indentationLevelForRowAt indexPath: IndexPath) -> Int {
        return 0
    }
    
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: "SectionHeader") as? SectionHeader
        
        switch section {
        case 0:
            cell?.titleLabel.text = "Pay"
        case 1:
            cell?.titleLabel.text = "Clubcard"
        case 2:
            cell?.titleLabel.text = "Transactions"
        default:
            cell?.titleLabel.text = "?"
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
}
