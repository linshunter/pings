//
//  BaseViewController.swift
//  PayQwiqer
//
//  Created by Gaurav Sharma on 29/03/2017.
//  Copyright © 2017 Gaurav Sharma. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.titleView = UIImageView(image: (#imageLiteral(resourceName: "PayQwiqHeader")))

    }
}
