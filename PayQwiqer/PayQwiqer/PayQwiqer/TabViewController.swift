//
//  TabViewController.swift
//  PayQwiqer
//
//  Created by Gaurav Sharma on 29/03/2017.
//  Copyright © 2017 Gaurav Sharma. All rights reserved.
//

import UIKit

class TabViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        selectedIndex = 1
    }
}
