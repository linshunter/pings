//
//  TutorialPageViewController.swift
//  prototype2
//
//  Created by Gaurav Sharma on 17/02/2017.
//  Copyright © 2017 PayQwiq. All rights reserved.
//

import UIKit
//import Lottie

class TutorialPageViewController: UIPageViewController {
    
    let pageCount = 2

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setViewControllers([tutorialStepForPage(page: 0)], direction: .forward, animated: false, completion: nil)

        dataSource = self
        
        view.backgroundColor = UIColor(red: 0, green: 178/255, blue: 234/255, alpha: 1)
    }

    func tutorialStepForPage(page: Int) -> TutorialStepViewController {
        
        let tutorialStepViewController = storyboard!.instantiateViewController(withIdentifier: "TutorialStepViewController") as! TutorialStepViewController
        
        tutorialStepViewController.page = page
        tutorialStepViewController.parentPageViewController = self //todo: ensure no reference cycle

        return tutorialStepViewController
    }
    
    func nextButtonPressed (currentPage: Int) {
        
        if currentPage < pageCount - 1 {
            let nextPage = tutorialStepForPage(page: currentPage + 1)
            setViewControllers([nextPage], direction: .forward, animated: true, completion: nil)
        }
    }
}

extension TutorialPageViewController : UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        return moveToNextPage(viewController: viewController)
    }

    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        return moveToPreviousPage(viewController: viewController)
    }
    
    func moveToNextPage (viewController: UIViewController) -> UIViewController? {
        if let currentTutorialStepController = viewController as? TutorialStepViewController {
            if currentTutorialStepController.page < pageCount - 1 {
                return tutorialStepForPage(page: currentTutorialStepController.page + 1)
            }
        }
        return nil
    }
    
    func moveToPreviousPage (viewController: UIViewController) -> UIViewController? {
        if let currentTutorialStepController = viewController as? TutorialStepViewController {
            if currentTutorialStepController.page > 0 {
                return tutorialStepForPage(page: currentTutorialStepController.page - 1)
            }
        }
        return nil
    }
}
