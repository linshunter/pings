//
//  ViewController.swift
//  prototype2
//
//  Created by Gaurav Sharma on 16/02/2017.
//  Copyright © 2017 PayQwiq. All rights reserved.
//

import UIKit
//import Lottie

class TutorialStepViewController: UIViewController {

    
    @IBOutlet weak var animationPlaceholderView: UIView!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var headerLabel: UILabel!
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var parentPageViewController: TutorialPageViewController!

    @IBOutlet weak var button: UIButton!
    
    var animationView: LOTAnimationView!
    
    var page = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch (page) {
        case 0:
            headerLabel.text = "Card added successfully"
            descriptionLabel.text = "Your card will be displayed in PayQwiq as a QR code"
            pageControl.currentPage = page
            animationView = LOTAnimationView.animationNamed("data_V3")
            button.setTitle("Next", for: .normal)
            button.setTitleColor(UIColor.white, for: .normal)
            button.backgroundColor = UIColor(red: 0, green: 193/255, blue: 240/255, alpha: 1)
        case 1:
            headerLabel.text = "Scan at the till to pay"
            descriptionLabel.text = "Scan the QR code to pay and collect clubcard points"
            pageControl.currentPage = page
            animationView = LOTAnimationView.animationNamed("TwitterHeart")
            button.setTitle("OK, got it!", for: .normal)
        default:
            print ("Page does not exist")
        }
        
        animationView.contentMode = .scaleAspectFit

        playAnimation()

        button.layer.cornerRadius = 6
        button.clipsToBounds = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(playAnimation))
        animationView.addGestureRecognizer(tapGesture)
    }
    
    override func viewDidLayoutSubviews() {
        animationView.frame = CGRect(x: 0, y: 0,
                                     width: animationPlaceholderView.frame.width,
                                     height: animationPlaceholderView.frame.height)
        
        animationPlaceholderView.addSubview(animationView!)
    }
    
    func playAnimation() {
        animationView.animationProgress = 0
        animationView.loopAnimation = true
        animationView?.play(completion: { (finished) in

        })
    }
    
    @IBAction func buttonTouchUp(_ sender: Any) {
        parentPageViewController.nextButtonPressed(currentPage:page)
    }
}
