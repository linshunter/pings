//
//  ViewController.swift
//  scrolling-form
//
//  Created by Gaurav Sharma on 06/07/2017.
//  Copyright © 2017 Custom Arts. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var innerViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var pinkPanelHeight: NSLayoutConstraint!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    var open:Bool = false
    
    var baseHeight:CGFloat = 600
    var panelHeightClosed:CGFloat = 20
    var panelHeightOpen:CGFloat = 280
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateInnerViewHeight()
        startObservingKeyboardEvents()
    }

    @IBAction func buttonPressed(_ sender: Any) {
        
        if open {
            open = false
            pinkPanelHeight.constant = panelHeightClosed
        } else {
            open = true
            pinkPanelHeight.constant = panelHeightOpen
        }
        
        updateInnerViewHeight()
    }
    
    func updateInnerViewHeight () {
        if open {
            innerViewHeight.constant = baseHeight + panelHeightOpen
        } else {
            innerViewHeight.constant = baseHeight + panelHeightClosed
        }
    }
    
    
    private func startObservingKeyboardEvents() {
        NotificationCenter.default.addObserver(self, selector:#selector(self.keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object:nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object:nil)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            if let keyboardRect = userInfo[UIKeyboardFrameEndUserInfoKey] {
                let keyboardSize = (keyboardRect as! CGRect).size
                
                scrollView.contentInset = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0)
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        scrollView.contentInset = UIEdgeInsets.zero
    }
}

