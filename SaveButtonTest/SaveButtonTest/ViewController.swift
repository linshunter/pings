//
//  ViewController.swift
//  SaveButtonTest
//
//  Created by Gaurav Sharma on 28/04/2017.
//  Copyright © 2017 Gaurav Sharma. All rights reserved.
//

import UIKit
//import Material


class ViewController: UITableViewController, UITextFieldDelegate {
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var textField2: UITextField!
    @IBOutlet weak var textField3: UITextField!
    @IBOutlet weak var textField4: UITextField!
    @IBOutlet weak var textField5: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textField.delegate = self
        textField2.delegate = self
        textField3.delegate = self
        textField4.delegate = self
        textField5.delegate = self
        
        textField.autocorrectionType = .no
        textField2.autocorrectionType = .no
        textField3.autocorrectionType = .no
        textField4.autocorrectionType = .no
        textField5.autocorrectionType = .no
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func actionPressMe(sender: UIButton!) {
        self.view.resignFirstResponder()
    }
}

