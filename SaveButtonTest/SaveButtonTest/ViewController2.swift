//
//  ViewController.swift
//  SaveButtonTest
//
//  Created by Gaurav Sharma on 28/04/2017.
//  Copyright © 2017 Gaurav Sharma. All rights reserved.
//

import UIKit
//import Material


class ViewController2: UITableViewController, UITextFieldDelegate {
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var textField2: UITextField!
    @IBOutlet weak var textField3: UITextField!
    @IBOutlet weak var textField4: UITextField!
    @IBOutlet weak var textField5: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let saveButton = UIButton(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width - 40, height: 40))
        saveButton.setTitle("Save", for: .normal)
        saveButton.backgroundColor = UIColor.blue
        saveButton.layer.cornerRadius = 5
        
        saveButton.addTarget(self, action: #selector(actionPressMe), for:.touchUpInside)
        
        let barButtonItem = UIBarButtonItem(customView: saveButton)
        
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
        toolBar.barStyle = UIBarStyle.default
        toolBar.items = [barButtonItem]
        
        textField.delegate = self
        textField2.delegate = self
        textField3.delegate = self
        textField4.delegate = self
        textField5.delegate = self
        
        textField.inputAccessoryView = toolBar
        textField2.inputAccessoryView = toolBar
        textField3.inputAccessoryView = toolBar
        textField4.inputAccessoryView = toolBar
        textField5.inputAccessoryView = toolBar
        
        textField.autocorrectionType = .no
        textField2.autocorrectionType = .no
        textField3.autocorrectionType = .no
        textField4.autocorrectionType = .no
        textField5.autocorrectionType = .no
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func actionPressMe(sender: UIButton!) {
        self.view.resignFirstResponder()
    }
}
