//
//  AppDelegate.swift
//  ViperProject
//
//  Created by Robert Gacsi on 18/08/2016.
//  Copyright © 2016 Robert Gacsi. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    var rootWireframe: Wireframe?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {

        if self.window == nil {
        
            self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        }
        
        self.rootWireframe = MainNavigationWireframe()
        
        self.window?.rootViewController = UINavigationController(rootViewController: self.rootWireframe!.viewController())
        self.window?.makeKeyAndVisible()
        
        return true
    }
}

