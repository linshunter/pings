//
//  SecondPageView.swift
//  ViperProject
//
//  Created by Robert Gacsi on 18/08/2016.
//  Copyright © 2016 Robert Gacsi. All rights reserved.
//

import Foundation

protocol SecondPageView: class {
    
    var eventHandler: SecondPageEventHandler? {get set}
}