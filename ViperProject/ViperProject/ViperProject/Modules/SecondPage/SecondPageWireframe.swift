//
//  SecondPageWireframe.swift
//  ViperProject
//
//  Created by Robert Gacsi on 18/08/2016.
//  Copyright © 2016 Robert Gacsi. All rights reserved.
//

import Foundation
import UIKit

class SecondPageWireframe {
    
    private var presenter: SecondPagePresenter?
    
    private var view = SecondPageViewController()
    
    private weak var mainRouter: protocol<MainNavigationRouter>?
    
    init(mainRouter: protocol<MainNavigationRouter>?) {
        
        self.presenter = SecondPagePresenter(view: self.view, router: self)
        
        self.view.eventHandler = presenter
        
        self.mainRouter = mainRouter
    }
}

// MARK: - Wireframe protocol methods

extension SecondPageWireframe: Wireframe {
    
    func viewController() -> UIViewController {
        
        return self.view
    }
}

// MARK: - SecondPageRouter protocol methods

extension SecondPageWireframe: SecondPageRouter {
    
    func showFirstPage() {
        
        self.view.navigationController?.popViewControllerAnimated(true)
    }
    
    func showMainPage() {
        
        self.mainRouter?.showMainPage()
    }
}