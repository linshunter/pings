//
//  SecondPagePresenter.swift
//  ViperProject
//
//  Created by Robert Gacsi on 18/08/2016.
//  Copyright © 2016 Robert Gacsi. All rights reserved.
//

import Foundation

class SecondPagePresenter {
    
    weak var view: SecondPageView?
    
    private weak var router: protocol<SecondPageRouter>?
    
    init(view: SecondPageView?, router: protocol<SecondPageRouter>?) {
        
        self.view = view
        
        self.router = router
    }
}

extension SecondPagePresenter: SecondPageEventHandler {

    func firstPageButtonTapped() {
        
        self.router?.showFirstPage()
    }
    
    func mainPageButtonTapped() {
        
        self.router?.showMainPage()
    }
}