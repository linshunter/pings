//
//  FirstPagePresenter.swift
//  ViperProject
//
//  Created by Robert Gacsi on 18/08/2016.
//  Copyright © 2016 Robert Gacsi. All rights reserved.
//

import Foundation

class FirstPagePresenter {
    
    weak var view: FirstPageView?
    
    private weak var router: protocol<FirstPageRouter>?
    
    private let dataInteractor = FirstPageDataInteractor()
    
    init(view: FirstPageView?, router: protocol<FirstPageRouter>?) {
        
        self.view = view
        
        self.router = router
        
        self.dataInteractor.presenter = self
    }
}

extension FirstPagePresenter: FirstPageEventHandler {

    func showTextButtonTapped() {
        
        self.dataInteractor.loadText()
    }
    
    func previousPageButtonTapped() {
        
        self.router?.showPreviousPage()
    }
    
    func nextPageButtonTapped() {
        
        self.router?.showNextPage()
    }
}

extension FirstPagePresenter: FirstPageDataInteractorResult {

    func loadText(text: String) {
        
        self.view?.showText(text)
    }
}