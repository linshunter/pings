//
//  FirstPageDataInteractor.swift
//  ViperProject
//
//  Created by Robert Gacsi on 18/08/2016.
//  Copyright © 2016 Robert Gacsi. All rights reserved.
//

import Foundation

protocol FirstPageDataInteractorResult: class {

    func loadText(text: String)
}

class FirstPageDataInteractor {

    weak var presenter: FirstPageDataInteractorResult?
    
    func loadText() {
    
        self.presenter?.loadText("BEER!!!")
    }
}