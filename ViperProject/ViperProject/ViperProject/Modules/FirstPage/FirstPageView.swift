//
//  FirstPageView.swift
//  ViperProject
//
//  Created by Robert Gacsi on 18/08/2016.
//  Copyright © 2016 Robert Gacsi. All rights reserved.
//

import Foundation

protocol FirstPageView: class {
    
    var eventHandler: FirstPageEventHandler? {get set}
    
    func showText(text: String)
}