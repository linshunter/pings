//
//  FirstPageWireframe.swift
//  ViperProject
//
//  Created by Robert Gacsi on 18/08/2016.
//  Copyright © 2016 Robert Gacsi. All rights reserved.
//

import Foundation
import UIKit

class FirstPageWireframe {
    
    private var presenter: FirstPagePresenter?
    
    private var view = FirstPageViewController()

    private weak var mainRouter: protocol<MainNavigationRouter>?
    
    init(mainRouter: protocol<MainNavigationRouter>?) {
        
        self.presenter = FirstPagePresenter(view: self.view, router: self)
        
        self.view.eventHandler = presenter
        
        self.mainRouter = mainRouter
    }
}

// MARK: - Wireframe protocol methods

extension FirstPageWireframe: Wireframe {
    
    func viewController() -> UIViewController {
        
        return self.view
    }
}

// MARK: - FirstPageRouter protocol methods

extension FirstPageWireframe: FirstPageRouter {

    func showPreviousPage() {
        
        self.view.navigationController?.popViewControllerAnimated(true)
    }
    
    func showNextPage() {
        
        self.mainRouter?.showSecondPage()
    }
}