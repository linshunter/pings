//
//  MainNavigationWireframe.swift
//  ViperProject
//
//  Created by Robert Gacsi on 18/08/2016.
//  Copyright © 2016 Robert Gacsi. All rights reserved.
//

import Foundation
import UIKit

class MainNavigationWireframe {
    
    private var presenter: MainNavigationPresenter?
    
    private var view = MainPageViewController()
    
    private var firstPageWireframe: FirstPageWireframe?
    private var secondPageWireframe: SecondPageWireframe?
        
    init() {
    
        self.presenter = MainNavigationPresenter(view: self.view, router: self)
        
        self.view.eventHandler = presenter
    }
    
    // MARK: - Public methods
    
    func push(wireframe: Wireframe, animated: Bool) {
    
        self.view.navigationController?.pushViewController(wireframe.viewController(), animated: animated)
    }
}

// MARK: - Wireframe protocol methods

extension MainNavigationWireframe: Wireframe {

    func viewController() -> UIViewController {
        
        return self.view
    }
}

// MARK: - MainNavigationRouter protocol methods

extension MainNavigationWireframe: MainNavigationRouter {

    func showMainPage() {
        
        self.view.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    func showFirstPage() {
        
        self.firstPageWireframe = FirstPageWireframe(mainRouter: self)
        
        self.push(self.firstPageWireframe!, animated: true)
    }
    
    func showSecondPage() {
        
        self.secondPageWireframe = SecondPageWireframe(mainRouter: self)
        
        self.push(self.secondPageWireframe!, animated: true)
    }
}