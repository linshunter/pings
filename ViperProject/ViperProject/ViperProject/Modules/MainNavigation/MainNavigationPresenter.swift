//
//  MainNavigationPresenter.swift
//  ViperProject
//
//  Created by Robert Gacsi on 18/08/2016.
//  Copyright © 2016 Robert Gacsi. All rights reserved.
//

import Foundation

class MainNavigationPresenter {
    
    weak var view: MainNavigationView?
    
    private weak var router: protocol<MainNavigationRouter>?
    
    init(view: MainNavigationView?, router: protocol<MainNavigationRouter>?) {
    
        self.view = view
        
        self.router = router
    }
}

extension MainNavigationPresenter: MainNavigationEventHandler {

    func firstPageButtonTapped() {
        
        self.router?.showFirstPage()
    }
}