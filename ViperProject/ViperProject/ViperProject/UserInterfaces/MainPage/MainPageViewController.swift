//
//  MainPageViewController.swift
//  ViperProject
//
//  Created by Robert Gacsi on 18/08/2016.
//  Copyright © 2016 Robert Gacsi. All rights reserved.
//

import UIKit

class MainPageViewController: UIViewController, MainNavigationView {

    var eventHandler: MainNavigationEventHandler?
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.title = "Main Page"
    }
    
    // MARK: - Actions
    
    @IBAction func didTouchUpInsideFirstPageButton(sender: UIButton) {
        
        self.eventHandler?.firstPageButtonTapped()
    }
}
