//
//  SecondPageViewController.swift
//  ViperProject
//
//  Created by Robert Gacsi on 18/08/2016.
//  Copyright © 2016 Robert Gacsi. All rights reserved.
//

import UIKit

class SecondPageViewController: UIViewController, SecondPageView {

    var eventHandler: SecondPageEventHandler?
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        self.title = "Second Page"
    }
    
    // MARK: - Actions
    
    @IBAction func didTouchUpInsideFirstPageButton(sender: UIButton) {
        
        self.eventHandler?.firstPageButtonTapped()
    }
    
    @IBAction func didTouchUpInsideMainPageButton(sender: UIButton) {
        
        self.eventHandler?.mainPageButtonTapped()
    }
}
