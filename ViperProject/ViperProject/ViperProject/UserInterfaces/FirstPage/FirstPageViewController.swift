//
//  FirstPageViewController.swift
//  ViperProject
//
//  Created by Robert Gacsi on 18/08/2016.
//  Copyright © 2016 Robert Gacsi. All rights reserved.
//

import UIKit

class FirstPageViewController: UIViewController, FirstPageView {
    
    var eventHandler: FirstPageEventHandler?
    
    @IBOutlet weak var textLabel: UILabel!
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.title = "First Page"
    }
    
    // MARK: - Actions
    
    @IBAction func didTouchUpInsideShowTextButton(sender: UIButton) {
        
        self.eventHandler?.showTextButtonTapped()
    }
    
    @IBAction func didTouchUpInsidePreviousPageButton(sender: UIButton) {
        
        self.eventHandler?.previousPageButtonTapped()
    }
    
    @IBAction func didTouchUpInsideNextPageButton(sender: UIButton) {
        
        self.eventHandler?.nextPageButtonTapped()
    }
    
    // MARK: - FirstPageView protocol methods
    
    func showText(text: String) {
        
        self.textLabel.text = text
    }
}
