import UIKit
import LiquidFloatingActionButton

class ViewController: UIViewController, LiquidFloatingActionButtonDataSource, LiquidFloatingActionButtonDelegate {
    
    var cells: [LiquidFloatingCell] = []
    var floatingActionButton: LiquidFloatingActionButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.titleView = UIImageView(image: UIImage(named: "Tesco Pay"))
        
        self.view.backgroundColor = UIColor.white

        let createLiquidButton: (CGRect, LiquidFloatingActionButtonAnimateStyle) -> LiquidFloatingActionButton = { (frame, style) in
            let floatingActionButton = CustomDrawingActionButton(frame: frame)
            floatingActionButton.animateStyle = style
            floatingActionButton.dataSource = self
            floatingActionButton.delegate = self
            return floatingActionButton
        }
        
        cells.append(CustomCell(icon: #imageLiteral(resourceName: "ic_cloud"), name: "  Profile" ))
        cells.append(CustomCell(icon: #imageLiteral(resourceName: "ic_system"), name: "  Transactions" ))
        cells.append(CustomCell(icon: #imageLiteral(resourceName: "ic_place"), name: "  Stores" ))
        
        let margin = CGFloat(20)
        let iconSize = CGFloat(56.0)
        
        let rightButton = createLiquidButton(CGRect(x: self.view.frame.width - iconSize - margin,
                                                    y: self.view.frame.height - iconSize - margin,
                                                    width: iconSize, height: iconSize), .up)
        
        rightButton.image = UIImage(named: "ic_launcher")

        self.view.addSubview(rightButton)
        
        let centerButton = createLiquidButton(CGRect(x: self.view.frame.width/2 - iconSize/2,
                                                    y: self.view.frame.height - iconSize - margin,
                                                    width: iconSize, height: iconSize), .up)
        
        self.view.addSubview(centerButton)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tap.numberOfTapsRequired = 1
        centerButton.addGestureRecognizer(tap)
        centerButton.isUserInteractionEnabled = true
    }
    
    func handleTap() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)

        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let action1 = UIAlertAction(title: "Profile", style: .default, handler: { (action) in
            let vc = storyboard.instantiateViewController(withIdentifier: "Profile")
            self.show(vc, sender: self)
        })
        let action2 = UIAlertAction(title: "Transactions", style: .default, handler: { (action) in
            let vc = storyboard.instantiateViewController(withIdentifier: "Transactions")
            self.show(vc, sender: self)
        })
        let action3 = UIAlertAction(title: "Stores", style: .default, handler: { (action) in
            let vc = storyboard.instantiateViewController(withIdentifier: "Stores")
            self.show(vc, sender: self)
        })
        let action4 = UIAlertAction(title: "Settings", style: .default, handler: { (action) in
            let vc = storyboard.instantiateViewController(withIdentifier: "Settings")
            self.show(vc, sender: self)
        })
        
        self.present(alertController, animated: true, completion:{
            alertController.view.superview?.subviews[1].isUserInteractionEnabled = true
            alertController.view.superview?.subviews[1].addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.handleTapDismiss)))
        })
        
        alertController.addAction(action1)
        alertController.addAction(action2)
        alertController.addAction(action3)
        alertController.addAction(action4)
    }
    
    func handleTapDismiss() {
        self.dismiss(animated: true, completion: nil)
    }

    func numberOfCells(_ liquidFloatingActionButton: LiquidFloatingActionButton) -> Int {
        return cells.count
    }
    
    func cellForIndex(_ index: Int) -> LiquidFloatingCell {
        return cells[index]
    }
    
    func liquidFloatingActionButton(_ button: LiquidFloatingActionButton, didSelectItemAtIndex index: Int) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        switch(index) {
        case 1:
            let vc = storyboard.instantiateViewController(withIdentifier: "Transactions")
            self.show(vc, sender: self)
        case 0:
            let vc = storyboard.instantiateViewController(withIdentifier: "Profile")
            self.show(vc, sender: self)
        default:
            let vc = storyboard.instantiateViewController(withIdentifier: "Stores")
            self.show(vc, sender: self)
        }
        
        button.close()
    }
}
