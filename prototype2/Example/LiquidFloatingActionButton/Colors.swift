import Foundation
import UIKit

class Colors {
    
    static let nileBlue = UIColor(26, 59, 80)
    static let lochmara = UIColor(4, 124, 184)
    
    static let cerulean = UIColor(0, 176, 230)
    static let pictonBlue = UIColor(64, 195, 236)
    
    static let onahau = UIColor(210, 243, 255)
    static let regentGrey = UIColor(130, 148, 159)
    
    static let athensGrey = UIColor(230, 233, 237)
    static let blackSqueeze = UIColor(235, 241, 248)
    
    static let alabaster = UIColor(250, 250, 250, 0.9)
    static let white = UIColor(255, 255, 255)
    
    static let mantis = UIColor(149, 211, 136)
    static let mandy = UIColor(227, 86, 92)
    
    static let yellowOrange = UIColor(253, 191, 70)
    static let tescoRed = UIColor(220, 14, 24)
    
    static let clubcardBlue = UIColor(1, 81, 129)
    static let black = UIColor(0, 0, 0)
    
    static let tescoDarkBlue = UIColor(0, 83, 159)

}
