import UIKit
import LiquidFloatingActionButton

public class CustomCell : LiquidFloatingCell {
    var name: String = "sample"
    
    init(icon: UIImage, name: String) {
        self.name = name
        super.init(icon: icon)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func setupView(_ view: UIView) {
        super.setupView(view)
        let label = UILabel()
        label.text = name
        label.textColor = Colors.tescoDarkBlue
        label.backgroundColor = Colors.white.withAlphaComponent(0.95)
        label.layer.cornerRadius = 15
        addSubview(label)
        label.snp.makeConstraints { make in
            make.left.equalTo(self).offset(-150)
            make.width.equalTo(135)
            make.top.height.equalTo(self)
        }
    }
}
