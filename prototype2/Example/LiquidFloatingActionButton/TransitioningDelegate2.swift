import UIKit

class TransitioningDelegate2: NSObject, UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        let sourceTableViewController = source.childViewControllers[1] as! ProfileTableViewController
        
        //let sourceCell = sourceTableViewController.tableView.cellForRow(at: sourceTableViewController.tableView.indexPathForSelectedRow!)!
        
        let flipPresentAnimationController = FlipPresentAnimationController()
        
        flipPresentAnimationController.originFrame = sourceTableViewController.tableView.frame
        
        return flipPresentAnimationController
    }
}
