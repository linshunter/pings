import UIKit

class FlipPresentAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
    
    var originFrame = CGRect()
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 1.0
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        let fromVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
        
        let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
        
        let toSnapshot = toVC.view.snapshotView(afterScreenUpdates: true)
        
        toSnapshot?.frame = originFrame
        toSnapshot?.layer.masksToBounds = true
        
        transitionContext.containerView.addSubview(toVC.view)
        
        toVC.view.isHidden = true
        
        transitionContext.containerView.addSubview(toSnapshot!)
        
        AnimationHelper.perspectiveTransformForContainerView(containerView: transitionContext.containerView)
        
        toSnapshot?.layer.transform = AnimationHelper.yRotation(angle: .pi/2)
        
        let duration = transitionDuration(using: transitionContext)
        
        UIView.animateKeyframes(
            withDuration: duration,
            delay: 0,
            options: .calculationModeCubic,
            animations: {
                UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 1/3, animations: {
                    fromVC.view.layer.transform = AnimationHelper.yRotation(angle: -.pi/2) })
                UIView.addKeyframe(withRelativeStartTime: 1/3, relativeDuration: 1/3, animations: {
                    toSnapshot?.layer.transform = AnimationHelper.yRotation(angle: 0.0) })
                UIView.addKeyframe(withRelativeStartTime: 2/3, relativeDuration: 1/3, animations: {
                    toSnapshot?.frame = transitionContext.finalFrame(for: toVC) })
        },
            completion: { _ in
                toVC.view.isHidden = false
                fromVC.view.layer.transform = AnimationHelper.yRotation(angle: 0.0)
                toSnapshot?.removeFromSuperview()
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        })
    }
}
