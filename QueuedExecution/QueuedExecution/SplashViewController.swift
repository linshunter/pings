//
//  SplashViewController.swift
//  QueuedExecution
//
//  Created by Simon Lawrence on 24/01/2017.
//  Copyright © 2017 Tesco Personal Finance Plc. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
    
    @IBOutlet weak var progressView: UIProgressView?
    
    let startupQueue = SerialQueue("com.tescobank.MyAwesomeQueue")
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.beginStartupOperations()
    }
    
    func updateProgress() {

        guard Thread.isMainThread else {
            return
        }
        
        guard let progressView = self.progressView else {
            return
        }
        
        progressView.setProgress(progressView.progress + 0.2, animated: true)
    }
    
    func startApp() {
        
        guard Thread.isMainThread else {
            return
        }
        
        self.performSegue(withIdentifier: "Ready", sender: nil)
    }
    
    func beginStartupOperations() {
        
        debugPrint("Sending startup operations to the queue...")
        
        weak var weakSelf = self
        
        // We have five startup tasks, A, B, C, D & E
        
        startupQueue.async({
            debugPrint("A: Working...")
            
            sleep(1)
            
            debugPrint("A: Done")
            
        }, completion: {
            
            weakSelf?.updateProgress()
        })
        
        startupQueue.async({
            debugPrint("B: Working...")
            
            sleep(1)
            
            debugPrint("B: Done")
            
        }, completion: {
            
            weakSelf?.updateProgress()
        })
        
        startupQueue.async({
            debugPrint("C: Working...")
            
            sleep(1)
            
            debugPrint("C: Done")
            
        }, completion: {
            
            weakSelf?.updateProgress()
        })
        
        startupQueue.async({
            debugPrint("D: Working...")
            
            sleep(1)
            
            debugPrint("D: Done")
            
        }, completion: {
            
            weakSelf?.updateProgress()
        })
        
        startupQueue.async({
            debugPrint("E: Working...")
            
            sleep(1)
            
            debugPrint("E: Done")
            
        }, completion: {
            
            weakSelf?.updateProgress()
            
            // Once E is done, we can start the application
            weakSelf?.startupQueue.mainQueueAsync {
                weakSelf?.startApp()
            }
        })
        
        debugPrint("Sent all that stuff, I'm out!")
    }
        
}

