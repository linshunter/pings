//
//  SerialQueue.swift
//  Payqwiq
//
//  Created by Simon Lawrence on 26/01/2017.
//  Copyright © 2017 Tesco Personal Finance Plc. All rights reserved.
//

import UIKit

class SerialQueue {
    
    #if swift(>=3.0)
    
    private let internalQueue: DispatchQueue
    
    init(_ label: String) {
        
        internalQueue = DispatchQueue(label: label, qos: .default, attributes: DispatchQueue.Attributes(), autoreleaseFrequency: DispatchQueue.AutoreleaseFrequency.workItem, target: DispatchQueue.global(qos: .default))
    }
    
    func async(_ code: @escaping () -> () ) {
        
        let item = DispatchWorkItem(block: code)
        
        self.internalQueue.async(execute: item)
    }
    
    func async(_ code: @escaping () -> (), completion: @escaping () -> ()) {
        
        self.async {
            code()
            
            DispatchQueue.main.async(execute: completion)
        }
    }
    
    func sync(_ code: @escaping () -> () ) {
        
        let item = DispatchWorkItem(block: code)
        
        self.internalQueue.sync(execute: item)
    }
    
    func mainQueueAsync(_ code: @escaping () -> () ) {
        
        DispatchQueue.main.async(execute: code)
    }
    
    func mainQueueSync(_ code: @escaping () -> () ) {
        
        guard !Thread.isMainThread else {
            
            code()
            
            return
        }
        
        DispatchQueue.main.sync(execute: code)
    }
    
    #else
    
    private let internalQueue: dispatch_queue_t
    
    init (_ label: String) {
    
        internalQueue = dispatch_queue_create(label.cStringUsingEncoding(NSUTF8StringEncoding)!, DISPATCH_QUEUE_SERIAL)
        dispatch_set_target_queue(internalQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, UInt(0)))
    }
    
    func async(code: () -> () ) {
    
        dispatch_async(self.internalQueue, code)
    }
    
    func async(code: () -> (), completion: () -> ()) {
    
        dispatch_async(self.internalQueue) {
    
            code()
    
            dispatch_async(dispatch_get_main_queue(), completion)
        }
    }
    
    func sync(code: () -> ()) {
    
        dispatch_sync(self.internalQueue, code)
    }
    
    func mainQueueAsync(code: () -> ()) {
    
        dispatch_async(dispatch_get_main_queue(), code)
    }
    
    func mainQueueSync(code: () -> () ) {
    
        guard !NSThread.isMainThread() else {
    
            code()
    
            return
        }
    
        dispatch_sync(dispatch_get_main_queue(), code)
    }
    
    #endif
}
