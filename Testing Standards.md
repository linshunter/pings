#Testing Standards##Triple A Syntax
Tests must conform to the triple A syntax of Arrange, Act, Assert.


    func testSetupButtonCreatesButtonAsExpected() {
    	
    	//Arrange
    	let button = TBButton(frame: CGRect.zero)
    	
    	//Act
    	button.setupButton()
    	
    	//Assert
    	XCTAssertEquals(button.titleLabel.text, "Click me", "The button was not setup as expected")
    }

Test signatures should describe what is being tested, in what state tests are conducted and the expected result.
    
    func testValidateTextFieldWhenPostcodeIsInvalidReturnsFalse() {
        
        //Test stuff
    }<br>
##Assertion Length
Multiple assertions can be used per test case, however tests cases should test only one element, function etc.

    func testInitaliseTextFieldSetupsTextfieldnAsExpected() {
    	
    	//Arrange
    	let textfield = TBTextfield()
    	
    	//Act
    	textfield.initalise()
    	
    	//Assert
    	XCTAssertEquals(textfield.placeholderText, "Email address", "Placeholder text should be 'Email Address'")
    	XCTAssertFalse(textfield.hidden, "Email Address textfield should not be hidden")
    }<br>
##Wrong, Right, Wrong, Right Pattern of Change.When making a change to the codebase the developer should use a Wrong, Right, Wrong, Right Pattern of Change (WRWR).
This pattern is as follows:
    Wrong  
    Codebase does not have tests covering the current implementation
    
    Right
    Developer backfills tests to preserve current implmentation
    
    Wrong
    Developer writes failing tests to cover change to codebase
    
    Right
    Developer writes enough code to makes tests pass for change
  
Where possible, developers should make changes using TDD.<br>##Marks and documentation
Test classes should use using pragma marks to create a publically available overview of the class.

Comments should be kept to an absolute minimum. 

Test function signatures should use explicit names and be written using correct prose. This will help to limit the number of comments in tests.

<br>##Touch and refactorWhen making a change, the developer has the duty to also carry out a refactor of the new code implemented and the class itself. 
Before any refactor a developer must make sure to adhere to the WRWR Pattern of Change.

<br>