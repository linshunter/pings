//
//  ViewController.swift
//  ClosestLocation
//
//  Created by Tim on 20/03/2018.
//  Copyright © 2018 Tesco Bank. All rights reserved.
//

import CoreLocation
import UIKit

enum FileLoaderError: Error {
    case FileNotFound
}

class ViewController: UIViewController {
    
    @IBOutlet weak var output: UILabel!
    
    private var locationManager: CLLocationManager?
    
    fileprivate var storeLocations: [StoreDetails] = []
    
    fileprivate var storeAtLongestDistance: StoreDetails?
    
    fileprivate var localStores: [StoreDetails] = []
    
    fileprivate var currentFurthestDistance: Double = 10000000
    
    fileprivate var startTime = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.locationManager = CLLocationManager()
        self.locationManager?.delegate = self
        self.locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager?.distanceFilter = kCLDistanceFilterNone
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.locationManager?.requestWhenInUseAuthorization()
        
        self.locationManager?.startUpdatingLocation()
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        
        self.output.text = "bugger too much data"
    }
}

extension ViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        manager.stopUpdatingLocation()
        
        if let currentLocation = locations.last {
            
            self.startTime = Date()
            
            self.getNearestStores(fromCurrentLocation: currentLocation)
        }
    }
    
    private func getNearestStores(fromCurrentLocation currentLocation: CLLocation) {
        
        let contents = self.loadFileContents()
        
        for line in contents {
            
            guard let storeDetails = self.buildStoreDetails(fromString: line) else {
                
                continue
            }

            self.handleStoreDetails(storeDetails: storeDetails, forCurrentLocation: currentLocation)
        }
        
        self.printStoreListOut()
    }
    
    private func loadFileContents() -> [String] {
        
        do {
            
            let filePath = try self.loadFile()
            
            let contents = try String(contentsOfFile: filePath)
            
            return contents.components(separatedBy: .newlines)
            
        } catch {
            
            return [""]
        }
    }
    
    private func loadFile() throws -> String {
        
        guard let filePath = Bundle.main.path(forResource: "AllStores", ofType: "csv") else {
            
            throw FileLoaderError.FileNotFound
        }
        
        return filePath
    }
    
    private func buildStoreDetails(fromString string: String) -> StoreDetails? {
        
        var lineData = string.components(separatedBy: ",")
        
        if let longitude = Double(lineData[2]), let latitude = Double(lineData[3]) {
            
            return StoreDetails(branchName: lineData[0], location: CLLocation(latitude: latitude, longitude: longitude))
        }
        
        return nil
    }
    
    private func printStoreListOut() {
        
        var outputString = ""
        
        for store in self.localStores {
            
            outputString.append(store.branchName + "\n")
        }
        
        let endTime = Date()
        
        let timeInterval = endTime.timeIntervalSince(startTime)
        
        outputString.append(String(describing: timeInterval))
        
        self.output.text = outputString
    }
    
    private func handleStoreDetails(storeDetails: StoreDetails, forCurrentLocation currentLocation: CLLocation) {
        
        if self.localStores.count < 20 {
            
            self.localStores.append(storeDetails)
        } else {
            
            let currentStoreDistance = currentLocation.distance(from: storeDetails.location)
            
            let furthestStore = self.getFurthestStore(fromCurrentLocation: currentLocation)
            
            let furthestStoreDistance = furthestStore.location.distance(from: currentLocation)
            
            if furthestStoreDistance > currentStoreDistance {
                
                self.localStores = self.localStores.filter({ (storeDetails: StoreDetails) -> Bool in
                    
                    return furthestStore.branchName != storeDetails.branchName
                })
                
                self.localStores.append(storeDetails)
            }
        }
    }
    
    private func getFurthestStore(fromCurrentLocation currentLocation: CLLocation) -> StoreDetails {
        
        var furthestStore = self.localStores.first!
        
        var furthestStoreDistance = furthestStore.location.distance(from: currentLocation)
        
        for localStore in self.localStores {
            
            let localStoreDistance = currentLocation.distance(from: localStore.location)
            
            if localStoreDistance > furthestStoreDistance {
                
                furthestStore = localStore
                furthestStoreDistance = localStoreDistance
            }
        }
        
        return furthestStore
    }
}

struct StoreDetails {
    
    var branchName: String
    var location: CLLocation
}

