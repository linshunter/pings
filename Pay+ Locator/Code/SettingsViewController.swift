//
//  SettingsViewController.swift
//  Pay Plus Locator
//
//  Created by Gaurav Sharma on 10/01/2018.
//  Copyright © 2018 Ken Toh. All rights reserved.
//

import UIKit
import CoreLocation

class SettingsViewController: UITableViewController {
    
    @IBOutlet weak var radiusLabel: UILabel!
    @IBOutlet weak var radiusSlider: UISlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func radiusSliderValueChanged(_ sender: Any) {
        
        radiusLabel.text = "Geofence Radius: " + String(Int(radiusSlider.value))
        GeoNotification.radius = CLLocationDistance(radiusSlider.value)
    }
}
