//
//  Helpers.swift
//  Pay Plus Locator
//
//  Created by Gaurav Sharma on 09/01/2018.
//  Copyright © 2018 Ken Toh. All rights reserved.
//

import Foundation
import CoreLocation

class Helpers {
    class func getGeoNotifications() -> [GeoNotification] {
        
        var geoNotifications = [GeoNotification]()
        
        //TODO: load full list from a data source
        
//        geoNotifications.append(GeoNotification("Tesco Bank EHQ", CLLocationCoordinate2D(latitude: 55.931760, longitude: -3.296095)))
        //geoNotifications.append(GeoNotification("CLERKENWELL RD SAINSBURY", CLLocationCoordinate2D(latitude: 51.522367, longitude: -0.107919)))
        geoNotifications.append(GeoNotification("Prufrock Coffee", CLLocationCoordinate2D(latitude: 51.520160, longitude: -0.109457)))
        geoNotifications.append(GeoNotification("FARRINGDON RD EXPRESS", CLLocationCoordinate2D(latitude: 51.520276, longitude: -0.105949)))
        geoNotifications.append(GeoNotification("SOUTH TOTTENHAM SUPERSTORE", CLLocationCoordinate2D(latitude: 51.58474, longitude: -0.07155)))
        geoNotifications.append(GeoNotification("TWICKENHAM EXTRA", CLLocationCoordinate2D(latitude: 51.45885, longitude: -0.33746)))
        geoNotifications.append(GeoNotification("BATTERSEA RD EXPRESS", CLLocationCoordinate2D(latitude: 51.46358, longitude: -0.18618)))
        geoNotifications.append(GeoNotification("DUNSTABLE EXTRA", CLLocationCoordinate2D(latitude: 51.88953, longitude: -0.48501)))
        geoNotifications.append(GeoNotification("BISHOPSGATE METRO", CLLocationCoordinate2D(latitude: 51.51722, longitude: -0.08031)))
        geoNotifications.append(GeoNotification("SURREY QUAYS EXTRA", CLLocationCoordinate2D(latitude: 51.49454, longitude: -0.04603)))
        geoNotifications.append(GeoNotification("CLAPHAM SUPERSTORE", CLLocationCoordinate2D(latitude: 51.45331, longitude: -0.1466)))
        geoNotifications.append(GeoNotification("STREATHAM PLACE EXPRESS", CLLocationCoordinate2D(latitude: 51.44693, longitude: -0.12786)))
        geoNotifications.append(GeoNotification("HACKNEY SUPERSTORE", CLLocationCoordinate2D(latitude: 51.54688, longitude: -0.05269)))
        geoNotifications.append(GeoNotification("SIDCUP SUPERSTORE", CLLocationCoordinate2D(latitude: 51.41258, longitude: 0.12288)))
        geoNotifications.append(GeoNotification("ENFIELD SOUTHBURY RD SUPERSTORE", CLLocationCoordinate2D(latitude: 51.65098, longitude: -0.07674)))
        geoNotifications.append(GeoNotification("HACKNEY SUPERSTORE", CLLocationCoordinate2D(latitude: 51.54688, longitude: -0.05269)))
        //    geoNotifications.append(GeoNotification("BULLS BRIDGE EXTRA", CLLocationCoordinate2D(latitude: 51.49902, longitude: -0.40766)))
        geoNotifications.append(GeoNotification("BALDOCK EXTRA", CLLocationCoordinate2D(latitude: 51.98504, longitude: -0.18622)))
        geoNotifications.append(GeoNotification("ROYSTON EXTRA", CLLocationCoordinate2D(latitude: 52.05923, longitude: -0.0328)))
        geoNotifications.append(GeoNotification("COATBRIDGE EXPRESS", CLLocationCoordinate2D(latitude: 55.85242, longitude: -3.99853)))
        geoNotifications.append(GeoNotification("OLYMPIA EXPRESS", CLLocationCoordinate2D(latitude: 51.4951, longitude: -0.21104)))
        geoNotifications.append(GeoNotification("HAMMERSMITH SUPERSTORE", CLLocationCoordinate2D(latitude: 51.49768, longitude: -0.22397)))
        geoNotifications.append(GeoNotification("CORSTORPHINE EXTRA", CLLocationCoordinate2D(latitude: 55.9405, longitude: -3.29511)))
        
        return geoNotifications
    }
}
