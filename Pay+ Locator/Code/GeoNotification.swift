import UIKit
import MapKit
import CoreLocation

class GeoNotification: NSObject, MKAnnotation {
    
    var coordinate: CLLocationCoordinate2D
    var identifier: String
    
    static var radius = CLLocationDistance(exactly: 60)
    
    init(_ identifier: String, _ coordinate: CLLocationCoordinate2D) {
        self.coordinate = coordinate
        self.identifier = identifier
    }
}
