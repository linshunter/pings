import UIKit
import MapKit
import CoreLocation
import UserNotifications

class MapViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    
    var geotifications: [GeoNotification] = []
    
    var locationManager = CLLocationManager()
    
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var bottomLabel: UILabel!
    @IBOutlet weak var errorLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.locationManager.delegate = self
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        self.locationManager.startUpdatingLocation()
        self.locationManager.allowsBackgroundLocationUpdates = true
        
        if #available(iOS 10.0, *) {
            
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in
                if granted {
                    DispatchQueue.main.async {
                        self.errorLabel.text = "Notifications Access Granted"
                        self.errorLabel.backgroundColor = UIColor.orange
                    }
                }
                else {
                    if let error = error {
                        DispatchQueue.main.async {
                            self.errorLabel.text = error.localizedDescription
                            self.errorLabel.backgroundColor = UIColor.red
                        }
                    }
                }
            }
            
            UNUserNotificationCenter.current().getNotificationSettings() { notificationSettings in
                print("getNotificationSettings: \(notificationSettings)")
            }
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.mapView.zoomToUserLocation()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupGeoNotifications()
    }
    
    func setupGeoNotifications() {
        
        mapView.removeOverlays(mapView.overlays)
        
        for region in self.locationManager.monitoredRegions {
            locationManager.stopMonitoring(for: region)
        }
        
        if !CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
            showAlert(withTitle:"Error", message: "Geofencing is not supported on this device!")
            return
        }

        if CLLocationManager.authorizationStatus() != .authorizedAlways {
            showAlert(withTitle:"Warning", message: "Your geotification is saved but will only be activated once you grant Geotify permission to access the device location.")
        }
        
        for geoNotification in Helpers.getGeoNotifications() {
            
            mapView?.add(MKCircle(center: geoNotification.coordinate, radius: GeoNotification.radius!))
            
            let region = CLCircularRegion(center: geoNotification.coordinate,  radius: GeoNotification.radius!, identifier: geoNotification.identifier)
            
            self.locationManager.startMonitoring(for: region)
        }
    }
    
    @IBAction func zoomToCurrentLocation(sender: AnyObject) {
        mapView.zoomToUserLocation()
    }
}

extension MapViewController: CLLocationManagerDelegate {
    
    //MARK: Location Manager events
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        if region is CLCircularRegion {
            
            if #available(iOS 10.0, *) {
                
                let content = UNMutableNotificationContent()
                content.title = region.identifier
                content.body = "Remember to use Tesco Pay+ today at this store."
                content.sound = UNNotificationSound.default()
                
                let request = UNNotificationRequest(identifier: region.identifier, content: content, trigger: nil)
                
                UNUserNotificationCenter.current().add(request) { (error : Error?) in
                    if let error = error {
                        self.errorLabel.text = error.localizedDescription
                        self.errorLabel.backgroundColor = UIColor.red
                    }
                }
            }
        }
        
        bottomLabel.text = "Entered Geofence of region: \(region)"
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        }
        
        bottomLabel.text = "Exited Geofence of region: \(region)"
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        mapView.showsUserLocation = (status == .authorizedWhenInUse || status == .authorizedAlways)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        topLabel.text = locations.description
    }
    
    //MARK: Location Manager errors
    func mapView(_ mapView: MKMapView, didFailToLocateUserWithError error: Error) {
        errorLabel.backgroundColor = .red
        errorLabel.text = "Failed to locate: " + String(describing: error)
    }
    
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        errorLabel.backgroundColor = .red
        errorLabel.text = "Monitoring failed for: \(String(describing: region)), error:\(error)"
    }
}

extension MapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKCircle {
            let circleRenderer = MKCircleRenderer(overlay: overlay)
            circleRenderer.lineWidth = 1.0
            circleRenderer.strokeColor = .red
            circleRenderer.fillColor = UIColor.red.withAlphaComponent(0.4)
            return circleRenderer
        }
        return MKOverlayRenderer(overlay: overlay)
    }
}

@available(iOS 10.0, *)
extension MapViewController: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        errorLabel.backgroundColor = .orange
        errorLabel.text = "willPresent " + notification.debugDescription
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        errorLabel.backgroundColor = .orange
        errorLabel.text = "didReceive " + response.debugDescription
    }
}
